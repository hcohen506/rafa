({
    doInit : function(component, event, helper) {
		var action = component.get("c.getEvents");
		action.setParam("selectedDate", component.get("v.selectedDate"));
		action.setCallback(this, function (response) {
			var state = response.getState();
			if (component.isValid() && state === "SUCCESS") {
				var returnValue = JSON.parse(response.getReturnValue())
				component.set("v.events", returnValue["mEventList"]);
				component.set("v.dayOfWeek", returnValue["mDayOfWeek"]);
			}
		});
		$A.enqueueAction(action);


    }
})