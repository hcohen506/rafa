public with sharing class Cls_ObjectCreator {    
    public User thisUser;

    public Cls_ObjectCreator() {
        thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];       
    }   

        public Profile createProfile(String prof){
          Profile p = [SELECT Id, Name FROM Profile WHERE Name=:prof];
          return p; 
        }    
        
        public UserRole createUserRole(String userRoleName){
            UserRole r = new UserRole(name = userRoleName);
            System.runAs (thisUser) {
                insert r;  
            }
                return r;
        }
        
         public User createUserWithManager(Profile p, String  usrRl, user Manageruser ) {
            User user  = new User();
            if (!string.isblank (usrRl) &&  usrRl != null){
                UserRole usrRole = createUserRole(usrRl);
                user.Userroleid = usrRole.id;           
            }
            user.managerid = Manageruser.id;
            user.LastName = 'TestUser';
            user.Alias = 'TestUser';
            user.Email = 'TestUser@CloudtechUser.com';
            user.Username = 'testuser' + getRandomString(5) + '@testing.com';
            user.ProfileId = p.Id;
            user.TimeZoneSidKey = 'America/Los_Angeles';
            user.LanguageLocaleKey = 'en_US';
            user.LocaleSidKey = 'en_US';
            user.EmailEncodingKey = 'UTF-8';
            user.IsActive = true;
            insert user;
            return user;
        }

        
        public User createUser(Profile p, String  usrRl ) {
            User user  = new User();
            if (!string.isblank (usrRl) &&  usrRl != null){
                UserRole usrRole = createUserRole(usrRl);
                user.Userroleid = usrRole.id;           
            }
            user.LastName = 'TestUser';
            user.Alias = 'TestUser';
            user.Email = 'TestUser@CloudtechUser.com';
            user.Username = 'testuser' + getRandomString(5) + '@testing.com';
            user.ProfileId = p.Id;
            user.TimeZoneSidKey = 'America/Los_Angeles';
            user.LanguageLocaleKey = 'en_US';
            user.LocaleSidKey = 'en_US';
            user.EmailEncodingKey = 'UTF-8';
            user.IsActive = true;
            insert user;
            return user;
        }

        public User createSystemAdministratorUser(){
            User u = new User();
            u.Username = 'testuser' + getRandomString(5) + '@testing.com';
            u.Email = u.Username;
            u.ProfileId = [Select id from Profile where Name = 'System Administrator' Limit 1].id;
            u.Alias =  getRandomString(8);
            u.LastName = 'TestMan';
            u.CommunityNickname = getRandomString(15);
            u.IsActive = true;        
            u.EmailEncodingKey = 'UTF-8';
        u.TimeZoneSidKey = 'Europe/London';
        u.LanguageLocaleKey = 'en_US';
        u.LocaleSidKey = 'en_GB';        
        return u;
    } 

    public Account CreateParentAccount(){
        Account ac = new Account(
            Name = 'Accounttest1',
            Type = 'Other',
            Territory__c ='1',
            Status__c = 'Active');
        System.runAs (thisUser) {
            insert ac;                  
        }
        return ac;
    }

    public Account CreateAccountWithParentAccount(Account acc){
        Account ac = new Account(
            ParentId = acc.id,
            Belongs_to__c = 'לאומית',
            Name = 'Accounttest1',
            Type = 'Other',
            Territory__c ='1',
            Status__c = 'Active');
        System.runAs (thisUser) {
            insert ac;                  
        }
        //insert ac ;  
        return ac; 
    }
    
    public Contact createContact(Account acc){
            integer randomint = getRandomInteger();
            integer randomint2 = getRandomInteger();
            Contact con = new Contact(
                AccountId = acc.id,
                LastName = 'Contactest1',
                Contact_Type__c = 'אחר',
                Expertise__c = 'PCR',
                Email = 'Contactest1@gmail.com',
                Interests__c = 'CNV',
                MobilePhone = '05251143'+ string.valueof(randomint)+string.valueof(randomint2));

        System.runAs (thisUser) {
            insert con;  
        }
            return con; 
    }

    public Objective__c createObjective(user u) {
        date dt = system.today();
        Objective__c obj = new Objective__c(
            Objective_Description__c='test', 
            Required_No_Of_Visits__c=9,
            Classification__c = 'A',
            Experties__c = 'PCR',
            Territory__c ='1',
            Year__c = string.valueof(dt.year()),
            Quarter__c = '1',
            Representative__c=u.id);
        System.runAs (thisUser) {
            insert obj;
        }
        return obj;
    }

    public AC_Relationship__c createAC_Relationship (Account acc, Contact con) {
            AC_Relationship__c aCRelationship = new AC_Relationship__c(
                Contact__c=con.id, 
                Account__c=acc.id,
                Classification__c = 'A',
                Expertise__c = 'PCR',
                Position__c ='מנהל מחלקה');
            System.runAs (thisUser) {
                insert aCRelationship;
            }
            return aCRelationship;
    }

    public Target__c createTarget (Objective__c obj, user u, AC_Relationship__c aCRelationship ) {
        date dt = system.today();
        Target__c trg = new Target__c(
            Objective__c = obj.id,
            Representative__c=u.id,
            Year__c = string.valueof(dt.year()),
            Quarter__c = '1',
            Related_Account__c = aCRelationship.id,
            Required_No_Of_Visits__c = 10, 
            Contact__c = aCRelationship.Contact__c,
            Classification__c = 'A',
            Experties__c = 'PCR' );
        System.runAs (thisUser) {
            insert trg;
        }
        return trg;
    }

    public Conference__c creatConference () {
           date confDt = system.today();
        Conference__c conference = new Conference__c(
            Name = 'testConference' + getRandomString(5) ,
            Conference_Type__c = 'PM',
            Conf_Description__c = 'Test_ConfDescription' ,
            Conference_Date__c = confDt);
        System.runAs (thisUser) {
            insert conference;
        }
        return conference;
    }

     public Conf_Participant__c creatConferenceParticipant (Contact cont, Conference__c conf ) {
        Conf_Participant__c confParticipant = new Conf_Participant__c(
            Contact__c = cont.id,
            Conference__c = conf.id);
        System.runAs (thisUser) {
            insert confParticipant;
        }
        return confParticipant;
     }

    public Event createEventNotStarted(target__c thisWhatId, string  RcdtypeName, id usrId, String status ) {
        datetime dt = system.now();
        id eventRecordTypeId = Schema.SObjectType.Event.getRecordTypeInfosByName().get(RcdtypeName).getRecordTypeId();  
        Event evn = new Event (RecordTypeId = eventRecordTypeId, 
                               status__c = 'Not started',
                               whatid = thisWhatId.id,
                               WhoId = thisWhatId.Contact__c,
                               Related_Account__c = thisWhatId.Related_Account__c,
                               Subject = 'Meeting' ,
                               Type= 'Meeting',
                               OwnerId = usrId,
                               StartDateTime = dt,
                               Classification__c = 'R',
                               EndDateTime = dt.addHours(2));
        System.runAs (thisUser) {
            insert evn;
        }
        return evn;
    }

      public Event createEventCompleted (target__c thisWhatId, string  RcdtypeName, id usrId ) {
        datetime dt = system.now();
        /*List<String> pickListValuesList= new List<String>();
        Schema.DescribeFieldResult fieldResult = Event.Utilization_Level__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
            pickListValuesList.add(pickListVal.getValue());
        }   
        system.debug('pickListValuesList: ' + pickListValuesList);*/
        id eventRecordTypeId = Schema.SObjectType.Event.getRecordTypeInfosByName().get(RcdtypeName).getRecordTypeId();  
        List<String> pickListValuesList= new List<String>();
        Schema.DescribeFieldResult fieldResult = Event.Main_Product__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        String mainProd = ple[0].getvalue();
        for( Schema.PicklistEntry pickListVal : ple){
            if (pickListVal.getvalue().containsIgnoreCase('AXI'))
                mainProd = pickListVal.getvalue();
            //system.debug('*** Label: ' + pickListVal.getLabel() + ';; Val:' + pickListVal.getvalue());
        }      

        Event evn = new Event (RecordTypeId = eventRecordTypeId, 
                               status__c = 'Completed',                               
                               //Main_Product__c = 'Epiduo',
                               Main_Product__c = mainProd,
                               Meeting_Summary__c  = 'My Meeting Summary',
                               Description = 'My Meeting Description', 
                               Subscription_Potential__c  = 'נמוך' ,                            
                               Utilization_Level__c = 'רושם' ,                               
                               whatid = thisWhatId.id,
                               WhoId = thisWhatId.Contact__c,
                               Related_Account__c = thisWhatId.Related_Account__c,
                               Subject = 'Meeting' ,
                               Type= 'Meeting',
                               OwnerId = thisWhatId.Representative__c,
                               StartDateTime = dt,
                               Classification__c = 'R',
                               EndDateTime = dt.addHours(2));
        System.runAs (thisUser) {
            insert evn;
        }
        return evn;
    }

    public string getRandomString(integer len){
        string s = '';
        string alphabet = 'abcdefghijklmnopqrstuvwxyz';
        for(Integer i = 0; i < len; i++){
            integer randomint = getRandomInteger();
            if(!(randomint + 1 > alphabet.Length())) s = s + alphabet.substring(randomint, randomint + 1);
            else i--;
        }
        return s;
    }

    public integer getRandomInteger(){
        integer multiplier = (Math.round(Math.random()) == 1)?10 : 100;
        return Math.round(Math.random() * multiplier);
    }  
    
    public Objective__c createObjective(Id uId){
        Objective__c o = new Objective__c(representative__c = uId, Objective_Description__c = 'j', Required_No_Of_Visits__c = 1.0, Experties__c = 'PCR', Classification__c = 'A');
        insert o;
        return o;
    }

    public Objective__c returnObjective(Id uId){
        Objective__c o = new Objective__c(representative__c = uId, Objective_Description__c = 'j', Required_No_Of_Visits__c = 1.0, Experties__c= 'PCR', Classification__c = 'A');
        return o;
    }

    public Event createEvent(Id wid){//}, Id accId){
        Event ev = new Event(WhoId = wid, status__c  = 'Completed', Return_to_Contact__c = true, startDateTime = Datetime.newInstance(2008, 12, 1), DurationInMinutes = 10);//, AccountId = accId);
        insert ev;
        return ev;
    }   

    public Event returnEvent(Id wid){//}, Id accId){
        Event ev = new Event(WhoId = wid, status__c  = 'Completed', Return_to_Contact__c = true, startDateTime = Datetime.newInstance(2008, 12, 1), DurationInMinutes = 10);//, AccountId = accId);
        return ev;
    }

    public Target__c createTarget(){//connecting the target to a new created objective here
        Target__c t = new Target__c(Objective__c = this.createObjective(UserInfo.getUserId()).Id);
        insert t;
        return t;
    } 

    public Target__c returnTarget(){//connecting the target to a new created objective here
        Target__c t = new Target__c(Objective__c = this.createObjective(UserInfo.getUserId()).Id);
        return t;
    }

}