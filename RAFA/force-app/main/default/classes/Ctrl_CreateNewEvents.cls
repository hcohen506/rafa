public with sharing class Ctrl_CreateNewEvents {
    //public Event event {get;set;}
    public String pgSuccessMsg {get; set;}
    public String pgErrorMsg {get; set;}
    public String noEventsMsg {get; set;}
    public List<Event> eventsList {get;set;}
    public Boolean showEventTable {get; set;}
    public Boolean showTargetTable {get; set;}
    public Target__c trg {get;set;}
    public AC_Relationship__c accCon {get;set;}
    public String dayOfWeekMsg {get;set;}
    public String thisDayInWeek {get;set;}
    public Target__c contactLookUp {get;set;}
    public Target__c representativeLookUp {get;set;}


    public Date selectedDate {get;set;}
    public Integer thisYear {get;set;}
    public Integer thisQuarter {get;set;}
    public Decimal diffDays {get;set;}
    public Id eventRecordTypeId {get;set;}
    public String toSelectExpertise {get; set;}
    public String toSelectTerritory {get; set;}
    public String toSelectClassification {get; set;}
    public String toSelectCity {get; set;}
   // public List<String> toSelectInterests {get; set;} //String
    public Time selectedFromTime {get; set;} //String
    public Time selectedUntilTime {get; set;} //String
    public List<trgWrapper> trgWrpList {get;set;} 
    public Boolean isCheck_meetingSince {get; set;}   

    public Map<Id,List<myWrp>> ChildParentMap {get;set;}
    public List<myWrp> ParentList {get;set;}
    public List<myWrp> ChildList {get;set;}
    public String selectedParent {get;set;}
    public String selectedChild {get;set;}

    public string getItems2() {
        // return JSON.serialize(getChildWrp([SELECT Id, Name FROM account WHERE ParentId != null]));
        return JSON.serialize(getChildWrp());
    }

    public void setParamy() {
        System.debug('^^^ parent: '+selectedParent);
        System.debug('^^^ child: '+selectedChild);   
    }

    public string getItems1() {
        return JSON.serialize(getParentWrp([SELECT Id, Name FROM account WHERE ParentId = null]));
    }

    public string getItems3(String Idy) {
        return JSON.serialize(ChildParentMap.get(Idy));
    }

    public list<myWrp> getParentWrp (list<account> accList){
        if(ParentList == null){
            ParentList =  new  list<myWrp>();
            for(account a:accList){
                ParentList.add(new myWrp(a));
            }
        }
        return ParentList;
    }

    public list<myWrp> getChildWrp (){
        System.debug('inside Child, parent is:'+ selectedParent);
        if(selectedParent == null || selectedParent == '' || selectedParent == 'none'){
            return ChildList;
        }else{
            System.debug('ChildParentMap.get(selectedParent):'+ ChildParentMap.get(selectedParent));
            return ChildParentMap.get(selectedParent);
        }
    }


    public List<SelectOption> getExpertiesList(){
        return populatePickListOption('Target__c','Experties__c',' All '); 
    }
    public List<SelectOption> getTerritoryList(){
        return populatePickListOption('Account','Territory__c',' All ');   
    }
    public List<SelectOption> getClassificationList(){
        return populatePickListOption('Target__c','Classification__c',' All ');    
    }
    public List<SelectOption> getCityList(){
        return populatePickListOption('Account','City__c',' All ');    
    }
    //public List<SelectOption> getInterestsList(){       
    //    return populatePickListOption('Contact','Interests__c',null);   
    //}

    public Ctrl_CreateNewEvents() {
        contactLookUp = new Target__c();
        representativeLookUp = new  Target__c();
        isCheck_meetingSince = false;
        trgWrpList  = new List<trgWrapper> ();
        eventRecordTypeId = Schema.SObjectType.Event.getRecordTypeInfosByName().get('Meeting').getRecordTypeId();        
        diffDays = 0;
        showTargetTable = false;
        showEventTable = false;
        trg = new Target__c();
        accCon = new AC_Relationship__c();
        trg.Quarter_Start_Date__c = Date.today();
        selectedDate = trg.Quarter_Start_Date__c;
        Datetime dt = (DateTime)selectedDate;
        thisDayInWeek = dt.format('EEEE');
        String dayOfWeekMsg;

        selectedChild ='none';
        ChildParentMap = new Map<Id,List<myWrp>>();
        ParentList =  new List<myWrp>();
        ChildList = new List<myWrp>();
        List<Account> fullList = [SELECT Id, Name,ParentId FROM account];
        for (account a : fullList) {
            if(a.name == 'Rafa Fictitious'){ // this was requested by Gil for some reason
                continue;
            }
            if(a.ParentId == null){
                if(!ChildParentMap.containsKey(a.id)){
                    ChildParentMap.put(a.id, new List<myWrp>());
                }
                ParentList.add(new myWrp(a));
            }else{
                if(ChildParentMap.containsKey(a.ParentId)){
                    ChildParentMap.get(a.ParentId).add(new myWrp(a));
                }else{
                    ChildParentMap.put(a.ParentId, new List<myWrp>());
                    ChildParentMap.get(a.ParentId).add(new myWrp(a));
                }
                ChildList.add(new myWrp(a));
            }
        }

        clearPageMsg();
        GetDayOfWeek();
        //system.debug('*** TEST3 - thisDayInWeek: ' + thisDayInWeek);
    }

     public class myWrp{
        id id;
        string name;
        public myWrp(account a){
            this.id = a.id;
            this.name =  a.name;
        }
    }

    public class trgWrapper{
        public Target__c targt {get;set;}
        public Boolean isSelected {get;set;}
        public Boolean isIdentical {get;set;}
        //public String accountName {get;set;}
        //public String contactName {get;set;}
        //public String contactId {get;set;}
        //public String accntId {get;set;}
        //public String clinicSize {get;set;}
        //public String subscriptionPotential {get;set;}
        //public String utilizationLevel {get;set;}
        //public String position {get;set;}

        public trgWrapper(Target__c trgt , Boolean isSelected, Boolean isIdentical ){ 
            this.targt = trgt;
            this.isSelected = isSelected;
            this.isIdentical = isIdentical;
        }
    }
//void
    public PageReference   GetDayOfWeek(){
        //System.debug('GetDayOfWeek');
        showEventTable = false;
        showTargetTable = false;
        trgWrpList  = null;
        eventsList = null;
        noEventsMsg = null;
        if (trg.Quarter_Start_Date__c==null){
            dayOfWeekMsg = ' -- Select a Date! -- ';
            return null;
        }
        selectedDate = trg.Quarter_Start_Date__c;
        Date nextdDay = selectedDate.addDays(1);

    // Get day in week
        Datetime dt = (DateTime)selectedDate;
        thisDayInWeek = dt.format('EEEE');
        dayOfWeekMsg = 'The Day in Week Is '+ thisDayInWeek;
    //  Get Year and Quarter:
        SetYearAndQuarter();        
    // Get events List
        eventsList = new List<Event> ();
        eventsList =[select subject,owner.name,who.name,Related_Account__r.Account__r.name,Description,
                     Related_Account__r.Classification__c,startDateTime,whoId
                     from event 
                     where StartDateTime >: selectedDate and  StartDateTime <: nextdDay and
                     OwnerId=:userinfo.getUserId() and 
                     RecordTypeId  =: eventRecordTypeId
                     limit 50];
        //system.debug('*** eventsList: ' + eventsList + '; eventsList.isEmpty():' +eventsList.isEmpty() + '; eventsList.size():' +eventsList.size());
        if (eventsList != null && !eventsList.isEmpty() && eventsList.size()>0)
            showEventTable = true;
        else 
        {
            showEventTable = false;
            noEventsMsg = 'You have no events in this Selected Date';
        }
        return null;
    }

    public void SetYearAndQuarter(){
        thisYear = selectedDate.year();
        if (selectedDate.month() >= 1 && selectedDate.month() <= 3)
            thisQuarter = 1;
        else if (selectedDate.month() >= 4 && selectedDate.month() <= 6) {
            thisQuarter = 2;
        }
        else if (selectedDate.month() >= 7 && selectedDate.month() <= 9) {
            thisQuarter = 3;
        }
        else {
            thisQuarter = 4;
            
        }
    }

    public pagereference Search(){
        noEventsMsg = null;
        selectedDate = trg.Quarter_Start_Date__c;
        if (selectedDate == null){
            showEventTable = false;
            noEventsMsg = 'You have to select a date for creating an events';
            return null;
        }
        //system.debug('*** thisDayInWeek: ' + thisDayInWeek);
        if (thisDayInWeek == 'Saturday' && !Test.isRunningTest()){
            showEventTable = false;
            noEventsMsg = 'Saturday is not a valid work day';
            return null;
        }

        if ( (accCon.Sunday_From_1__c != null && accCon.Sunday_to_1__c  == null) ||
            (accCon.Sunday_From_1__c == null && accCon.Sunday_to_1__c  != null) ){
            showEventTable = false;
            noEventsMsg = 'You have to select a start time and end time for search';
            return null;
        }
        if (accCon.Sunday_From_1__c >= accCon.Sunday_to_1__c){
            showEventTable = false;
            noEventsMsg = 'Start time has to be earlier than end time';
            return null;
        }
        SetYearAndQuarter();
        selectedFromTime = accCon.Sunday_From_1__c;
        selectedUntilTime = accCon.Sunday_to_1__c;
        clearPageMsg();
        showTargetTable = false;        
        trgWrpList  = new List<trgWrapper> ();
    // Creating the String for the query:
        String qryString = 'select id,Objective__r.OwnerId,Related_Account__r.Account__c,Related_Account__r.Account__r.parent.name,Related_Account__r.Account__r.parentid,First_Day_of_Quarter__c,Quarter_Start_Date__c,Last_Event_Date__c,Representative__c,Representative__r.name,Contact__r.name,Related_Account__r.Friday__c,Related_Account__r.Thursday__c,Related_Account__r.Wednesday__c,Related_Account__r.Tuesday__c,Related_Account__r.Monday__c,Related_Account__r.Sunday__c ,Related_Account__r.account__r.name,Related_Account__r.Account__r.Territory__c,Related_Account__r.Account__r.City__c,Experties__c,Related_Account__r.position__c,Classification__c,Number_Of_Visits_Performed__c,Required_No_Of_Visits__c,Today_Max_Event_Date__c,Quarter__c,Year__c,Territory__c  from Target__c ';
               //qryString += ' where Experties__c  = \''+ toSelectExpertise +'\'' + ' and Territory__c  = \''+ toSelectTerritory  +'\'' +  ' and Classification__c  = \''+ toSelectClassification  +'\'';
        //String whereString;
        String whereString = ' Where Representative__c  = \''+ UserInfo.getUserId()  +'\'' ;    
        whereString += ' and Year__c     = \''+ string.valueof(thisYear) +'\'' +  ' and Quarter__c     = \''+ string.valueof(thisQuarter)  +'\'';
        if (toSelectExpertise != 'null' && toSelectExpertise != null)
            whereString += ' and Experties__c     = \''+ toSelectExpertise +'\'' ;
        if (toSelectTerritory != 'null' && toSelectTerritory != null)
            whereString += ' and Territory__c  = \''+ toSelectTerritory +'\'' ;
        if (toSelectClassification != 'null'&& toSelectClassification != null)
            whereString += ' and Classification__c  = \''+ toSelectClassification +'\'' ;        
        if (contactLookUp.contact__c != null)
            whereString += ' and Contact__c  = \''+ contactLookUp.contact__c +'\'' ;
        if (toSelectCity != 'null' && toSelectCity != null)
            whereString += ' and Related_Account__r.Account__r.City__c= \''+ toSelectCity +'\'' ;  
        if (selectedParent != 'none' && selectedParent != null)
            whereString += ' and Related_Account__r.Account__r.parentid  = \''+ selectedParent +'\'' ;   
        if (selectedChild != 'none' && selectedChild != null)
            whereString += ' and Related_Account__r.Account__c  = \''+ selectedChild +'\'' ;  

        //if (representativeLookUp.representative__c != null)
        //    whereString += ' and Representative__c  = \''+ representativeLookUp.representative__c +'\'' ;
        //else          
        //    whereString += ' and Representative__c  = \''+ UserInfo.getUserId()  +'\'' ;        

      /*  if (toSelectInterests != null && !toSelectInterests.isEmpty() && toSelectInterests.size()>0 )
        {
            //System.debug('*** toSelectInterests: ' + toSelectInterests);
            String newStr = '' ;
            for (string  intr : toSelectInterests){
                newStr += '\'' + intr.trim() + '\',';
            }
            newStr = newStr.Substring(0,newStr.length()-1); 
            whereString += ' and Contact__r.Interests__c  includes (' + newStr + ')';                
            //System.debug('*** newStr : ' + newStr);
        }
        */

        if (diffDays != null && diffDays > 0)
            whereString += ' and Today_Max_Event_Date__c  >= '+ diffDays;

        if ((selectedFromTime != null && !string.isblank(string.valueof(selectedFromTime))))
        {
            //EXample: Sunday_From_1__c;Sunday_From_2__c'   : 'Sunday_to_1__c;Sunday_To_2__c';
            //string relatedAccount_from = getAcRelTimeFields ('TypeFromTime',thisDayInWeek);
            //string fromTime1 = 'Related_Account__r.'+relatedAccount_from.substringBefore(';');
            //string fromTime2 = 'Related_Account__r.'+relatedAccount_from.substringAfter(';');
            //string relatedAccount_until = getAcRelTimeFields ('TypeUntilTime',thisDayInWeek);
            //string untilTime1 = 'Related_Account__r.'+relatedAccount_until.substringBefore(';');
            //string untilTime2 = 'Related_Account__r.'+relatedAccount_until.substringAfter(';');  
            if (thisDayInWeek != 'Saturday'){
                string fromTime1 = 'Related_Account__r.'+thisDayInWeek+'_From_1__c';
                string fromTime2 = 'Related_Account__r.'+thisDayInWeek+'_From_2__c';
                string untilTime1 = 'Related_Account__r.'+thisDayInWeek+'_to_1__c';
                string untilTime2 = 'Related_Account__r.'+thisDayInWeek+'_to_2__c';            
                whereString += ' and ( ( ('+ fromTime1 + '<=' + selectedFromTime + ' and '+ untilTime1 + '>=' + selectedUntilTime +')';
                whereString += 'or ('+ fromTime1 + '>=' + selectedFromTime + ' and '+ fromTime1 + '<=' + selectedUntilTime +')';
                whereString += 'or ('+ untilTime1 + '>=' + selectedFromTime + ' and '+ untilTime1 + '<=' + selectedUntilTime +') )';
                whereString += ' OR ( ('+ fromTime2 + '<=' + selectedFromTime + ' and '+ untilTime2 + '>=' + selectedUntilTime +')';
                whereString += 'or ('+ fromTime2 + '>=' + selectedFromTime + ' and '+ fromTime2 + '<=' + selectedUntilTime +')';
                whereString += 'or ('+ untilTime2 + '>=' + selectedFromTime + ' and '+ untilTime2 + '<=' + selectedUntilTime +')) )';
            }
        }
        System.debug('*** first qry: ' + qryString);
        System.debug('*** whereString: ' + whereString);

        //List<Target__c> trgSearch = [select Related_Account__r.Friday_From_1__c,id,Contact__r.name,Related_Account__r.account__r.name,Experties__c,
        //                             Related_Account__r.position__c,Classification__c,Number_Of_Visits_Performed__c,
        //                             Required_No_Of_Visits__c,Today_Max_Event_Date__c  from Target__c
        //                             where Experties__c =: toSelectExpertise and Territory__c =: toSelectTerritory and 
        //                             Classification__c =: toSelectClassification 
        //                             and Contact__r.Interests__c in: toSelectInterests 
        //                             and Today_Max_Event_Date__c >=: diffDays 
        //                             and Related_Account__r.Friday_From_1__c <=: accCon.Sunday_From_1__c
        //                             limit 100]; 
        
        //System.debug('*** qryString: ' + qryString);
        //if (whereString != null)
        //{
              //  whereString = whereString.replace('null and', ' where ');
                qryString = qryString + whereString + ' order by Classification__c,Number_Of_Visits_Performed__c ' + ' limit 251 ';
        //}
        System.debug('*** Final qry: ' + qryString);
        for (Target__c trgt : Database.query(qryString))
        {   
            if (isCheck_meetingSince && isCheck_meetingSince != null){
               // if (trgt.Quarter_Start_Date__c > trgt.Last_Event_Date__c)
               if (trgt.First_Day_of_Quarter__c  > trgt.Last_Event_Date__c || trgt.Last_Event_Date__c==null){
                    //trgWrpList.add(new trgWrapper(trgt,false));                 
                    if (trgt.Representative__c != trgt.Objective__r.OwnerId ){
                        trgWrpList.add(new trgWrapper(trgt,false,false));      
                    }
                    else 
                        trgWrpList.add(new trgWrapper(trgt,false,true));      
                    
               }
            }
            else{
                //trgWrpList.add(new trgWrapper(trgt,false)); 
                if (trgt.Representative__c != trgt.Objective__r.OwnerId ){
                    trgWrpList.add(new trgWrapper(trgt,false,false)); 
                }
                else {
                    trgWrpList.add(new trgWrapper(trgt,false,true));    
                }
                
            }
        }
        if (trgWrpList != null && !trgWrpList.isEmpty() && trgWrpList.size()>250)
        {   
            showTargetTable = false;
            pgSuccessMsg = null;
            pgErrorMsg = 'Too many records. Please filter and search again';    
        }
        else if (trgWrpList != null && !trgWrpList.isEmpty() && trgWrpList.size()>0)
            showTargetTable = true;
        else
        {
            showTargetTable = false;
            pgSuccessMsg = null;
            pgErrorMsg = 'No Target found <br/> Please change Search criteria ';
        }
        return null;
    }

    public void CreateEvent(){
        clearPageMsg();
         if (selectedDate == null){
            showEventTable = false;
            noEventsMsg = 'You have to select a date for creating an events';
        }
        if (showTargetTable){
            List<Event> eventsToInsert = new List<Event>();
            Datetime startDt = Datetime.newInstance(selectedDate, Time.newInstance(16,00,0,0));
            Datetime endDt = Datetime.newInstance(selectedDate, Time.newInstance(16,30,0,0)); 
            for (trgWrapper tr : trgWrpList)
            {
                if (tr.isSelected)
                {
                    system.debug(' *** Territory: ' + tr.targt.Related_Account__r.Account__r.Territory__c);
                    system.debug(' *** City: ' + tr.targt.Related_Account__r.Account__r.City__c);
                    // Target__c.Year__c
                    eventsToInsert.add( new Event ( RecordTypeId = eventRecordTypeId, Subject='Meeting' ,Type='פגישה', Related_Account__c=tr.targt.Related_Account__c, OwnerId  = userinfo.getUserId(), 
                                            whoid = tr.targt.Contact__c, whatid = tr.targt.id, StartDateTime = startDt, EndDateTime = endDt, Status__c = 'Not started', Classification__c = tr.targt.Classification__c, 
                                            Year__c = tr.targt.Year__c , Quarter__c = tr.targt.Quarter__c, Territory__c = tr.targt.Related_Account__r.Account__r.Territory__c, City__c = tr.targt.Related_Account__r.Account__r.City__c ));
                    // , City__c = tr.targt.Related_Account__r.Account__r.City__c
                }
            }
            try{
                if (eventsToInsert.size()>0)
                {
                    insert eventsToInsert;
                    //System.debug('event id: ' + eventsToInsert[0].id);
                    pgErrorMsg = null;
                    pgSuccessMsg = 'Event insert successfully';
                    trgWrpList.clear();
                    showTargetTable = false;
                    GetDayOfWeek();                  
                }
                else
                {
                    pgSuccessMsg = null;
                    pgErrorMsg = 'Select at least one record you want to create an event for';
                }
            }
            catch (Exception ex)
            {
                system.debug('*** System Exception:' + ex);
                pgSuccessMsg = null;
                pgErrorMsg = 'There was a problem with the creation process.';
            }
        }
        else 
        {
                pgSuccessMsg = null; 
                pgErrorMsg = 'Select at least one record you want to create an event for';
                return;             
        }
    }

    private List<Schema.PicklistEntry> getPicklistValues(String objName, String fieldName) {
        Schema.SObjectType t = Schema.getGlobalDescribe().get(objName);
        Schema.DescribeSObjectResult r = t.getDescribe();
        Schema.DescribeFieldResult statusFieldDescription = r.fields.getMap().get(fieldName).getDescribe();
        return statusFieldDescription.getPicklistValues();
    }

    public List<SelectOption> populatePickListOption(String obj, String fieldName,String additionalFirstVal){
        List<SelectOption> tempOptions = new List<SelectOption>();
        if (additionalFirstVal != null){
            tempOptions.add(new SelectOption('null', additionalFirstVal));
        }
        for(Schema.PicklistEntry field: getPicklistValues(obj, fieldName)){
            if(field.getValue() != null && field.getLabel() != null){
                tempOptions.add(new SelectOption(field.getValue(), field.getLabel()));
            }
        }           
        return tempOptions;
    }
    private void ClearPageMsg(){
        pgSuccessMsg= null;
        pgErrorMsg = null;
    }

    //private string getAcRelTimeFields(String typeTime, String weekDay){
    //    string fieldNames;
    //    if (weekDay =='Sunday')
    //        fieldNames = typeTime == 'TypeFromTime'  ? 'Sunday_From_1__c;Sunday_From_2__c'   : 'Sunday_to_1__c;Sunday_To_2__c';
    //    else if (weekDay =='Monday')
    //        fieldNames = typeTime == 'TypeFromTime'  ? 'Monday_From_1__c;Monday_From_2__c'   : 'Monday_to_1__c;Monday_To_2__c';
    //    else if (weekDay =='Tuesday')
    //        fieldNames = typeTime == 'TypeFromTime'  ? 'Tuesday_From_1__c;Tuesday_From_2__c' : 'Tuesday_to_1__c;Tuesday_To_2__c';
    //    else if (weekDay =='Wednesday')
    //        fieldNames = typeTime == 'TypeFromTime'  ? 'Wednesday_From_1__c;Wednesday_From_2__c'   : 'Wednesday_to_1__c;Wednesday_To_2__c';
    //    else if (weekDay =='Thursday')
    //        fieldNames = typeTime == 'TypeFromTime'  ? 'Thursday_From_1__c;Thursday_From_2__c'   : 'Thursday_to_1__c;Thursday_To_2__c';
    //    else 
    //        fieldNames = typeTime == 'TypeFromTime'  ? 'Friday_From_1__c;Friday_From_2__c' : 'Friday_to_1__c;Friday_To_2__c';
    //    return fieldNames;
    // }
}