public with sharing class Ctrl_CreateNewTargets {
    //public static integer loopInt = 0;
    public Boolean showTable {get; set;}
    public String pgSuccessMsg {get; set;}
    public String pgErrorMsg {get; set;}
    public List<AccConWrapper> accConWrpList {get;set;} 
    public String toSelectExpertise {get; set;}
    public String toSelectClinicSize {get; set;}
    public String toSelectSubscriptionPotential {get; set;}
    public String toSelectgUtilizationLevel {get; set;}
    public String toSelectClassification {get; set;}
    public String toSelectPosition {get; set;}
    public String toSelectCity {get; set;}
   // public String toSelectParentAcc  {get; set;}
   // public String toSelectAccount  {get; set;}
    public List<Objective__c> thisObjective {get;set;}
    public AC_Relationship__c accLookUp {get;set;}
    public AC_Relationship__c parentaccLookUp {get;set;}
    public AC_Relationship__c representativeLookUp {get;set;}
    public AC_Relationship__c contactLookUp {get;set;}
    public String objectiveId {get; set;}
  //  public String parentAccqryString;
  //  public String accqryString;
    //public Account acc {get;set;}
    //public List<SelectOption> ParentAccList {get; set;}
    // public String toSelectTerritory {get; set;}
    //public List<Target__c> targetList {get;set;}

    // $$  public String parentAccTyped  {get; set;}
    // $$   public String accTyped  {get; set;}
    public Map<Id,List<myWrp>> ChildParentMap {get;set;}
    public List<myWrp> ParentList {get;set;}
    public List<myWrp> ChildList {get;set;}
    public String selectedParent {get;set;}
    public String selectedChild {get;set;}

    
    
    public Ctrl_CreateNewTargets() {
        selectedChild ='none';

        accLookUp = new AC_Relationship__c();
        parentaccLookUp = new AC_Relationship__c();
        representativeLookUp = new AC_Relationship__c ();
        contactLookUp = new AC_Relationship__c ();
        //acc = new Account ();
        objectiveId = Apexpages.currentPage().getParameters().get('objectiveId');
        GetThisObjective();                 
        //List<SelectOption> relevantPatientList2 = new  List<SelectOption>();  
        //relevantPatientList2 =getRelevantPatientList();
        //system.debug('*** relevantPatientList2: ' + relevantPatientList2);
        ChildParentMap = new Map<Id,List<myWrp>>();
        ParentList =  new List<myWrp>();
        ChildList = new List<myWrp>();
        List<Account> fullList = [SELECT Id, Name,ParentId FROM account];
        for (account a : fullList) {
            if(a.name == 'Rafa Fictitious'){ // this was requested by Gil for some reason
                continue;
            }
            if(a.ParentId == null){
                if(!ChildParentMap.containsKey(a.id)){
                    ChildParentMap.put(a.id, new List<myWrp>());
                }
                ParentList.add(new myWrp(a));
            }else{
                if(ChildParentMap.containsKey(a.ParentId)){
                    ChildParentMap.get(a.ParentId).add(new myWrp(a));
                }else{
                    ChildParentMap.put(a.ParentId, new List<myWrp>());
                    ChildParentMap.get(a.ParentId).add(new myWrp(a));
                }
                ChildList.add(new myWrp(a));
            }
        }
        System.debug('childParent Map:' +ChildParentMap);
    } 

    public class myWrp{
        id id;
        string name;
        public myWrp(account a){
            this.id = a.id;
            this.name =  a.name;
        }
    }
    
    
    public class AccConWrapper{
        public AC_Relationship__c accnt {get;set;}
        public Boolean isSelected {get;set;}
        public Boolean isDouble {get;set;}
        public Decimal requiredNoOfVisits {get;set;}
        public String accountName {get;set;}
        public String contactName {get;set;}
        public String contactId {get;set;}
        public String accntId {get;set;}
        public String clinicSize {get;set;}
        public String subscriptionPotential {get;set;}
        public String utilizationLevel {get;set;}
        public String position {get;set;}

        public AccConWrapper(AC_Relationship__c ac , Boolean isSelected, Decimal reqNoOfVis, Boolean isDouble ){
            this.accnt = ac;
            this.accntId = ac.id;
            this.isSelected = isSelected;
            this.isDouble = isDouble;
            this.requiredNoOfVisits = reqNoOfVis ;
            this.accountName = ac.account__r.name;
            this.contactName = ac.contact__r.name;
            this.contactId = ac.contact__c;
            this.clinicSize = ac.clinic_Size__c;
            this.subscriptionPotential = ac.subscription_Potential__c;
            this.utilizationLevel = ac.utilization_Level__c;
            this.position = ac.position__c;
        }
    }

    public string getItems2() {
        // return JSON.serialize(getChildWrp([SELECT Id, Name FROM account WHERE ParentId != null]));
        return JSON.serialize(getChildWrp());

    }

    public void setParamy() {
        System.debug('^^^ parent: '+selectedParent);
        System.debug('^^^ child: '+selectedChild);   
    }

    public string getItems1() {
        return JSON.serialize(getParentWrp([SELECT Id, Name FROM account WHERE ParentId = null]));
    }

    public string getItems3(String Idy) {

        return JSON.serialize(ChildParentMap.get(Idy));
    }

     public list<myWrp> getParentWrp (list<account> accList){
        if(ParentList == null){
            ParentList =  new  list<myWrp>();
            for(account a:accList){
                ParentList.add(new myWrp(a));
            }
        }
        return ParentList;
    }

    public list<myWrp> getChildWrp (){
        System.debug('inside Child, parent is:'+ selectedParent);
        if(selectedParent == null || selectedParent == '' || selectedParent == 'none'){
            return ChildList;
        }else{
            System.debug('ChildParentMap.get(selectedParent):'+ ChildParentMap.get(selectedParent));
            return ChildParentMap.get(selectedParent);
        }
    }


    public void GetThisObjective(){ 
        thisObjective = new List<Objective__c>([select id,Representative__c,Territory__c,Quarter__c,Year__c,Required_No_Of_Visits_In_targets__c,Required_No_Of_Visits__c,Objective_Description__c 
                                            from Objective__c where id =: objectiveId limit 1 ]);          
    }
    private List<Schema.PicklistEntry> getPicklistValues(String objName, String fieldName) {
        Schema.SObjectType t = Schema.getGlobalDescribe().get(objName);
        Schema.DescribeSObjectResult r = t.getDescribe();
        Schema.DescribeFieldResult statusFieldDescription = r.fields.getMap().get(fieldName).getDescribe();
        return statusFieldDescription.getPicklistValues();
    }

    public List<SelectOption> populatePickListOption(String obj, String fieldName,String additionalFirstVal){
        List<SelectOption> tempOptions = new List<SelectOption>();
        if (additionalFirstVal != null){
            tempOptions.add(new SelectOption('null', additionalFirstVal));
        }
        for(Schema.PicklistEntry field: getPicklistValues(obj, fieldName)){
            if(field.getValue() != null && field.getLabel() != null){
                tempOptions.add(new SelectOption(field.getValue(), field.getLabel()));
            }
        }           
        return tempOptions;
    }

    //public PageReference reloadParentAccList() {
    //    getParentAccList();
    //    return null;
    ////}

    //public List<SelectOption> getParentAccList(){
    //    toSelectParentAcc = null;
    //    List<SelectOption> ParentAccList = new List<SelectOption>();           
    //    ParentAccList.add(new SelectOption('null',' All '));
    //    if (parentAccTyped != null)
    //        parentAccqryString = 'select id,name from account where parentid = null and name LIKE \'%' + parentAccTyped + '%\'  order by name limit 999';
    //    else            
    //        parentAccqryString ='select id,name from account where parentid = null order by name limit 999';
    //    for( Account acc :  Database.query(parentAccqryString) )
    //    {
    //        ParentAccList.add(new SelectOption(acc.id, acc.name));
    //    }  
    //    return ParentAccList;
    //}

    //public List<SelectOption> getAccList(){
    //    toSelectAccount = null;
    //    List<SelectOption> AccList = new List<SelectOption>();           
    //    AccList.add(new SelectOption('null',' All '));
    //    //System.debug(' *** toSelectParentAcc: ' + toSelectParentAcc);
    //    accqryString = 'select id,name from account where parentid  != null ';
    //    if (toSelectParentAcc == 'null' || toSelectParentAcc == null)
    //    {    
    //        if (accTyped != null)
    //            accqryString += ' and name LIKE \'%' + accTyped + '%\'  order by name limit 999 ';
    //        else
    //            accqryString += ' order by name limit 999 ';
    //    }
    //    else{    
    //        if (accTyped != null)
    //            accqryString += ' and parentid =: toSelectParentAcc and name LIKE \'%' + accTyped + '%\' order by name limit 999 ';
    //        else
    //             accqryString += ' and parentid =: toSelectParentAcc order by name limit 999 ';                          
    //    } 
    //    for( Account acc : Database.query(accqryString))
    //    {
    //        AccList.add(new SelectOption(acc.id, acc.name));
    //    }       
    //    return AccList;
    //}
    
    public List<SelectOption> getExpertiesList(){
        return populatePickListOption('AC_Relationship__c','Expertise__c',' All '); 
    }
    public List<SelectOption> getClinicSizeList(){
        return populatePickListOption('AC_Relationship__c','Clinic_Size__c',' All ');   
    }
    public List<SelectOption> getSubscriptionPotentialList(){
        return populatePickListOption('AC_Relationship__c','Subscription_Potential__c',' All ');    
    }
    public List<SelectOption> getUtilizationLevelList(){
        return populatePickListOption('AC_Relationship__c','Utilization_Level__c',' All '); 
    }
    public List<SelectOption> getClassificationList(){
        return populatePickListOption('AC_Relationship__c','Classification__c',' All ');    
    }
    public List<SelectOption> getPositionList(){
        return populatePickListOption('AC_Relationship__c','Position__c',' All ');  
    }
    public List<SelectOption> getTerritoryList(){
        return populatePickListOption('Account','Territory__c',' All ');    
    }
    public List<SelectOption> getCityList(){
        return populatePickListOption('Account','City__c',' All ');    
    }

    //public void ParentAccSearch(){
    //}

    public void Search(){
      //  System.debug( ' *** toSelectParentAcc : '  + toSelectParentAcc );
        showTable = false;
        clearPageMsg();
        accConWrpList  = new List<AccConWrapper> ();
        //system.debug('###  qryString1:  ' + qryString);
        //system.debug('###  qryString2:  ' + qryString);
        //qryString += ' where Expertise__c = \'' + toSelectExpertise +'\''+ ' and Clinic_Size__c = \''+ toSelectClinicSize +'\'' + ' and Subscription_Potential__c = \'' + toSelectSubscriptionPotential +'\'';
        //qryString += ' and Utilization_Level__c = \''+ toSelectgUtilizationLevel +'\'' + ' and Classification__c = \''+ toSelectClassification +'\'' + ' and Account__r.Territory__c = \''+ toSelectTerritory +'\'';
        String qryString = 'select id,Account__c,(select id,name,Contact__c,Year__c,Quarter__c,Experties__c,Representative__c from Related_Account_Target__r),Account__r.parent.name,Representative__c,Account__r.City__c,Representative__r.name,Expertise__c,Classification__c,Account__r.name,Contact__c,Contact__r.name,Clinic_Size__c,Subscription_Potential__c,Utilization_Level__c,Position__c from AC_Relationship__c';
        String whereString = ' where Status__c = \'Active\' ' ;

        //if (accLookUp.util_toAccount__c != null)
        //    whereString += ' and Account__c = \''+ accLookUp.util_toAccount__c +'\'' ;
        //if (parentaccLookUp.util_toAccount__c != null)
        //    whereString += ' and Account__r.parentid  = \''+ parentaccLookUp.util_toAccount__c +'\'' ;
        if (representativeLookUp.representative__c != null)
            whereString += ' and representative__c  = \''+ representativeLookUp.representative__c +'\'' ;

        if (contactLookUp.util_toContact__c != null)
            whereString += ' and Contact__c  = \''+ contactLookUp.util_toContact__c +'\'' ;


        if (toSelectExpertise != 'null' && toSelectExpertise != null)
            whereString += ' and Expertise__c  = \''+ toSelectExpertise +'\'' ;        
        if (toSelectClinicSize != 'null' && toSelectClinicSize != null)
            whereString += ' and Clinic_Size__c  = \''+ toSelectClinicSize +'\'' ;
        if (toSelectSubscriptionPotential != 'null' && toSelectSubscriptionPotential != null)
            whereString += ' and Subscription_Potential__c  = \''+ toSelectSubscriptionPotential +'\'' ;
        if (toSelectgUtilizationLevel != 'null' && toSelectgUtilizationLevel != null)
            whereString += ' and Utilization_Level__c  = \''+ toSelectgUtilizationLevel +'\'' ;
        if (toSelectClassification != 'null' && toSelectClassification != null)
            whereString += ' and Classification__c  = \''+ toSelectClassification +'\'' ;
        if (toSelectPosition != 'null' && toSelectPosition != null)
            whereString += ' and Position__c  = \''+ toSelectPosition +'\'' ;
       // if (toSelectTerritory != 'null' && toSelectTerritory != null)
      //      whereString += ' and Account__r.Territory__c  = \''+ toSelectTerritory +'\'' ;      
        if (toSelectCity != 'null' && toSelectCity != null)
            whereString += ' and Account__r.City__c= \''+ toSelectCity +'\'' ;    
        if (selectedParent != 'none' && selectedParent != null)
            whereString += ' and (Account__r.parentid  = \''+ selectedParent +'\'' + ' OR Account__c = \''+ selectedParent +'\'' + ')';   
        if (selectedChild != 'none' && selectedChild != null)
            whereString += ' and Account__c  = \''+ selectedChild +'\'' ;   
                    
        //system.debug('^^ qryString:  ' + qryString + '\n ;whereString: ' + whereString );
        //system.debug('*** accLookUp.util_toAccount__c: ' + accLookUp.util_toAccount__c + ' \n ; parentaccLookUp.util_toAccount__c:  ' + parentaccLookUp.util_toAccount__c);   
           /* if (whereString != null){
                whereString = whereString.replace('null and', ' where ');
                qryString = qryString + whereString;
            }*/
        qryString = qryString + whereString + ' limit 301 ';
        //return;       
        //system.debug('** loopInt ' + loopInt);
        //loopInt++;
        //system.debug('*** toSelectClinicSize:  ' + toSelectClinicSize + ' \n toSelectSubscriptionPotential: ' + toSelectSubscriptionPotential );      
        system.debug('*** Last qryString:  ' + qryString);
        system.debug('*** selectedParent :  ' + selectedParent + '; selectedChild :' +selectedChild  );
        Boolean isDouble;
        for (AC_Relationship__c acc : Database.query(qryString))
        {
            isDouble = false;
            for(Target__c tr : acc.Related_Account_Target__r){
                if (tr.Year__c == thisObjective[0].Year__c && tr.Quarter__c == thisObjective[0].Quarter__c && tr.Experties__c == acc.Expertise__c && tr.Contact__c == acc.Contact__c && tr.Representative__c == acc.Representative__c)
                    isDouble = true;
            }
            if (isDouble)
                accConWrpList.add(new AccConWrapper(acc,false,0,true));
            else
                accConWrpList.add(new AccConWrapper(acc,false,0,false));
        }   
        if (accConWrpList != null && !accConWrpList.isEmpty() && accConWrpList.size()>300)
        {
            showTable = false;
            pgSuccessMsg = null;
            pgErrorMsg = 'Too many records. Please filter and search again';    
        }
        else if (accConWrpList != null && !accConWrpList.isEmpty() && accConWrpList.size()>0)
            showTable = true;
        else
        {
            showTable = false;
            pgSuccessMsg = null;
            pgErrorMsg = 'No Records found in your search';
        }
    }

    public void CreateTarget(){
        clearPageMsg();
        if (showTable){
            List<Target__c> targetListToInsert = new List<Target__c>();
            for (AccConWrapper accnt : accConWrpList)
            {
                if (accnt.isSelected)
                {
                    if (accnt.requiredNoOfVisits != null && accnt.requiredNoOfVisits > 0 )
                    {
                        //Classification__c = toSelectClassification;Experties__c = toSelectExpertise,
                        targetListToInsert.add ( new Target__c (Related_Account__c = accnt.accntId, Contact__c = accnt.contactId, Required_No_Of_Visits__c = accnt.requiredNoOfVisits, 
                                                                Experties__c =  accnt.accnt.Expertise__c, Classification__c = accnt.accnt.Classification__c ,
                                                                Status__c='Active',Objective__c = thisObjective[0].id, Year__c = thisObjective[0].Year__c , 
                                                                Quarter__c =  thisObjective[0].Quarter__c, Representative__c = thisObjective[0].Representative__c ,
                                                                Territory__c = thisObjective[0].Territory__c));         
                    }
                    else 
                    {
                        pgSuccessMsg = null; 
                        pgErrorMsg = 'You are missing Required Number Of Visits in your selected records';
                        return;             
                    }
                }
            }
            try{
                if (targetListToInsert.size()>0)
                {
                    insert targetListToInsert;
                    pgErrorMsg = null;
                    pgSuccessMsg = 'Create insert successfully';
                    GetThisObjective();        
                    accConWrpList.clear();
                    showTable = false;                  
                }
                else
                {
                    pgSuccessMsg = null;
                    pgErrorMsg = 'Select at least one record you want to create';
                }
            }
            catch (Exception ex)
            {
                system.debug('*** System Exception:' + ex);
                pgSuccessMsg = null;
                pgErrorMsg = 'There was a problem with the create process.';
            }
        }
        else{
            pgErrorMsg = 'Search and find Objectives to create';    
            pgSuccessMsg = null;    
        }
    }

    private void ClearPageMsg(){
        pgSuccessMsg= null;
        pgErrorMsg = null;
    }
}