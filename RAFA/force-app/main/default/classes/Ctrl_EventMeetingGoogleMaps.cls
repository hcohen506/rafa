public with sharing class Ctrl_EventMeetingGoogleMaps {

    public Target__c trg {get;set;}
    public List<Event> eventsList {get;set;}
    public Date selectedDate {get;set;}
    public Boolean showMap {get; set;}
    public Boolean showWaypoints {get; set;}
    public String noEventsMsg {get; set;}
    public Id eventRecordTypeId {get;set;}
    public Date meetingDate {get;set;}
    public String billingCountry {get; set;}
    public String centerPoint {get; set;}
    public String waypointsLink {get; set;}

    //public String subject1 {get; set;}
    //public String subject2 {get; set;}
    //public String subject3 {get; set;}
//select id,BillingStreet,BillingCity,BillingState from account 
    public Ctrl_EventMeetingGoogleMaps() {
        billingCountry = 'Israel';
        //centerPoint= null;
        //Date myDate = Date.today();
        //meetingDate = Date.newInstance(myDate.day(), myDate.month(), myDate.year());
        eventRecordTypeId = Schema.SObjectType.Event.getRecordTypeInfosByName().get('Meeting').getRecordTypeId();
        showMap = false;
        showWaypoints = false;

        noEventsMsg = null;
        eventsList = new List<Event>();
        trg = new Target__c();
        meetingDate =Date.today();
        //trg.Quarter_Start_Date__c = Date.today();
        //selectedDate = trg.Quarter_Start_Date__c;

    }

    public void ShowMap(){
        showMap = false;
        showWaypoints = false;
        noEventsMsg = null;
        selectedDate = meetingDate;
        Date nextdDay = selectedDate.addDays(1);
        //system.debug('^^ selectedDate: ' + selectedDate + ' ;nextdDay:  ' + nextdDay );
        //system.debug('^^ userinfo.getUserId: ' + userinfo.getUserId() + ' ;eventRecordTypeId :  ' + eventRecordTypeId );
        eventsList = new List<Event> ();
        eventsList =[select id,startDateTime,subject,Related_Account__r.account__r.Billingaddress,Related_Account__r.account__r.BillingCity,Related_Account__r.account__r.BillingStreet,
                     Related_Account__r.account__r.BillingState,Related_Account__r.Account__r.name from event 
                     where StartDateTime >=: selectedDate and  StartDateTime <: nextdDay and 
                     OwnerId=:userinfo.getUserId()  and RecordTypeId  =: eventRecordTypeId order by startDateTime                  
                     limit 20];
        //for (EVENT evn : eventsList ){
        //    system.debug('***id: ' + evn.id);
        //    system.debug('***address: ' + evn.Related_Account__r.account__r.Billingaddress);
        //    system.debug('***BillingStreet: ' + evn.Related_Account__r.account__r.BillingStreet + ' ;BillingCity:'+evn.Related_Account__r.account__r.BillingCity+' ;BillingState: '+evn.Related_Account__r.account__r.BillingState);

        //}
        //system.debug('*** eventsList: ' + eventsList);
      
        if (eventsList != null && !eventsList.isEmpty() && eventsList.size()>0){
            integer eveLstSize =  eventsList.size()-1;
            system.debug('*** eventsList-size: ' + eventsList.size());
            centerPoint = string.valueof(eventsList[0].Related_Account__r.account__r.Billingaddress);
            showMap = true;
            noEventsMsg ='';            
            if (eventsList.size()>1){
                showWaypoints = true;
                //waypointsLink = 'https://www.google.com/maps/dir/?api=1&origin=Paris,France&destination=Cherbourg,France&travelmode=driving&waypoints=Versailles,France%7CChartres,France%7CLe+Mans,France%7CCaen,France';
                waypointsLink = 'https://www.google.com/maps/dir/?api=1&origin='+ eventsList[0].Related_Account__r.account__r.BillingStreet +','+ eventsList[0].Related_Account__r.account__r.BillingCity +','+billingCountry;
                waypointsLink += '&destination=' + eventsList[eveLstSize].Related_Account__r.account__r.BillingStreet +','+ eventsList[eveLstSize].Related_Account__r.account__r.BillingCity +','+billingCountry;
                for(integer i=1; i<eventsList.size()-1; i++){
                    if (i==1)
                        waypointsLink +=  '&waypoints=';
                    if (i>1)
                        waypointsLink +=  '|';
                    waypointsLink +=  eventsList[i].Related_Account__r.account__r.BillingStreet +','+ eventsList[i].Related_Account__r.account__r.BillingCity +','+billingCountry;
                    //waypointsLink +=  '&waypoints=HASADNA 13,TEL AVIV-YAFO|hashalechet 11,TEL AVIV-YAFO';
                }
                waypointsLink +=  '&travelmode=driving';
                // waypoints=Versailles,France%7CChartres,France%7CLe+Mans,France%7CCaen,France';

            }
            //subject1 = eventsList[0].subject;
            //if (eventsList.size()==2)
            //    subject2 = eventsList[1].subject;
            //if (eventsList.size()==3)
            //    subject3 = eventsList[2].subject;
        }
        else
        {
            showMap = false;
            showWaypoints = false;
            noEventsMsg = 'You have no events in this Selected Date';
        }
    }
}