public with sharing class Ctrl_EventReporting {
    public Ctrl_EventReporting() {

    }

	@AuraEnabled
	public static String getEvents(Date selectedDate){
		if(selectedDate == null){
			selectedDate = Date.today();
		}
		String eventTime = getEventTime(selectedDate);
		String dayOfWeek = GetThisdayOfWeek(selectedDate);
		List<Event> eventList = queryEvents(selectedDate);
		List<WrapperEvent> wrapperEventList = generateWrappers(eventList);

		WrapperData wd= new WrapperData();
		wd.mEventList = wrapperEventList;
		wd.mDayOfWeek = dayOfWeek;
		return JSON.serialize(wd);

		




/*
		ContactList =  new List<myWrp>();
        //ChildList = new List<myWrp>();
        List<Contact> fullList = [SELECT Id, Name FROM Contact];
        for (Contact a : fullList) {
        	ContactList.add(new myWrp(a));
        }
		eventRecordTypeId = Schema.SObjectType.Event.getRecordTypeInfosByName().get('Meeting').getRecordTypeId();
		GetDetailsForCtrl();

*/
	}

/*
	public void GetDetailsForCtrl(){
		isToShowSelectedTargetsMap=false;
		
		// pageNum = 1;
		// isEditable = false;
		isShowLastEvents = false;
		evn = new Event();
		// if (selectedDate==null){
		// 	evn.UtilDate__c = Date.today();

		// }else{
		// 	evn.UtilDate__c = selectedDate;
		// }
		// thisdayOfWeek = GetThisdayOfWeek(evn.UtilDate__c);
		todatDay =  system.today();
		nextdDay = todatDay.addDays(1);
		Time myTime = Time.newInstance(datetime.now().time().hour(), datetime.now().time().minute(), 0, 0);
		String myTimeStr = string.valueof(myTime);
		startsTime =  myTimeStr.left(5);
		myTimeStr = string.valueof(myTime.addHours(1));
		endsTime = myTimeStr.left(5);
		GetEvents();
	}

*/

/*
	public String GetThisdayOfWeek(date dateParam){
		Datetime dt = (DateTime)dateParam;	
		return dt.format('EEEE');
	}

*/

/*
	public PageReference GetEvents(){
		ClearPageMsg();
		selectedDate = evn.UtilDate__c;
		thisdayOfWeek = GetThisdayOfWeek(evn.UtilDate__c);
		evnWrprList  = new List<evnWrapper> ();	
		thisNextdDay =  evn.UtilDate__c.addDays(1);
		if (evn.UtilDate__c == Date.today()	){
			eventTime = 'Today';
		}
		else if (evn.UtilDate__c < Date.today()) {
			eventTime = 'PastDate';	
		} 
		else{
			eventTime = 'FutureDate';
		}
		Integer indexCounter = 0;
		mapSelectedEvent = new Map <integer,event> ();
		System.debug('~~~ evn.UtilDate__c: ' + evn.UtilDate__c);
		System.debug('~~~ thisnextdDay: ' + thisnextdDay);
		//related_Account__r.Sunday__c, related_Account__r.Monday__c, related_Account__r.Tuesday__c, related_Account__r.Wednesday__c, related_Account__r.Thursday__c, 
		for (Event evn : [SELECT id,utilStartTime__c,utilEndTime__c,startDateTime,endDateTime,related_Account__c,whoId,whatId,ownerId, classification__c, target__c,
						  Objective__c, Objective__r.Classification__c, target__r.Classification__c, Meeting_Collaboration_Code__c,
						  related_Account__r.Sunday_FromTo__c, related_Account__r.Monday_FromTo__c,  related_Account__r.Tuesday_FromTo__c, related_Account__r.Wednesday_FromTo__c, related_Account__r.Thursday_FromTo__c,
						  related_Account__r.Sunday__c, related_Account__r.Monday__c, related_Account__r.Tuesday__c, related_Account__r.Wednesday__c, related_Account__r.Thursday__c,  
						  related_Account__r.Sunday_From_1__c,	   related_Account__r.Sunday_To_1__c,     related_Account__r.Sunday_From_2__c, 	   related_Account__r.Sunday_To_2__c,
						  related_Account__r.Monday_From_1__c,	   related_Account__r.Monday_To_1__c, 	  related_Account__r.Monday_From_2__c, 	   related_Account__r.Monday_To_2__c,
						  related_Account__r.Tuesday_From_1__c,    related_Account__r.Tuesday_To_1__c,    related_Account__r.Tuesday_From_2__c,    related_Account__r.Tuesday_To_2__c,
						  related_Account__r.Wednesday_From_1__c,  related_Account__r.Wednesday_To_1__c,  related_Account__r.Wednesday_From_2__c,  related_Account__r.Wednesday_To_2__c,
						  related_Account__r.Thursday_From_1__c,   related_Account__r.Thursday_To_1__c,   related_Account__r.Thursday_From_2__c,   related_Account__r.Thursday_To_2__c,
						 subject,edit_permission__c,status__c,who.name,experties__c,related_Account_Name__c,description, main_Product__c, team__c,
						 secondary_Product__c, third_Product__c, target_Priority__c,next_Meeting_Target__c,target__r.experties__c,return_to_Contact__c,location,
						 side_Effects__c, side_Effects_Comments__c, side_Effect_Product__c, side_Effect_Dosage__c, side_Effect_Doctor_Consent__c,
						 side_Effect__r.call_To_Dr_Consent__c, side_Effect__r.comments__c, 
						 side_Effect__r.sE_Dosage__c,side_Effect__r.sE_Product__c,side_Effect__r.sE_Way_Of_Use__c								 
						  FROM event
						//   where StartDateTime >: evn.UtilDate__c and  StartDateTime <: thisnextdDay and
						//   WHERE StartDateTime >= :evn.UtilDate__c AND  StartDateTime < :thisnextdDay AND
						  WHERE DAY_ONLY(convertTimezone(StartDateTime)) >= :evn.UtilDate__c AND  DAY_ONLY(convertTimezone(StartDateTime)) < :thisnextdDay AND
								OwnerId=:userinfo.getUserId() AND 
								RecordTypeId  =: eventRecordTypeId AND 
								(status__c = 'Not started' OR status__c = 'Completed')
						  ]){		
			// TO DO TODO: add back to event where query: 
			//for (Event evn : [select id,status__c,who.name, Related_Account_Name__c from event 
			//                   where StartDateTime >: evn.UtilDate__c and  StartDateTime <: thisnextdDay and 
			//OwnerId=:userinfo.getUserId() and 
			//RecordTypeId  =: eventRecordTypeId and 
			//(status__c = 'Not started' OR status__c = 'Completed')
			//]){							
			evnWrprList.add(new evnWrapper(evn,indexCounter,false)); 
			mapSelectedEvent.put(indexCounter,evn);
			indexCounter ++;    
		}
		return null;
	}
*/

	private static List<Event> queryEvents(Date selectedDate){
		Date nextdDay = selectedDate.addDays(1);
		Id eventRecordTypeId = Schema.SObjectType.Event.getRecordTypeInfosByName().get('Meeting').getRecordTypeId();
		List<Event> eventList = [SELECT id, utilStartTime__c, utilEndTime__c, startDateTime, endDateTime, related_Account__c, whoId,
									whatId, ownerId, classification__c, target__c, Objective__c, Objective__r.Classification__c,
									target__r.Classification__c, Meeting_Collaboration_Code__c, related_Account__r.Sunday_FromTo__c,
									related_Account__r.Monday_FromTo__c,  related_Account__r.Tuesday_FromTo__c, related_Account__r.Wednesday_FromTo__c,
									related_Account__r.Thursday_FromTo__c, related_Account__r.Sunday__c, related_Account__r.Monday__c,
									related_Account__r.Tuesday__c, related_Account__r.Wednesday__c, related_Account__r.Thursday__c,
									related_Account__r.Sunday_From_1__c, related_Account__r.Sunday_To_1__c, related_Account__r.Sunday_From_2__c,
									related_Account__r.Sunday_To_2__c, related_Account__r.Monday_From_1__c, related_Account__r.Monday_To_1__c,
									related_Account__r.Monday_From_2__c, related_Account__r.Monday_To_2__c, related_Account__r.Tuesday_From_1__c,
									related_Account__r.Tuesday_To_1__c, related_Account__r.Tuesday_From_2__c, related_Account__r.Tuesday_To_2__c,
									related_Account__r.Wednesday_From_1__c, related_Account__r.Wednesday_To_1__c, related_Account__r.Wednesday_From_2__c,
									related_Account__r.Wednesday_To_2__c, related_Account__r.Thursday_From_1__c, related_Account__r.Thursday_To_1__c,
									related_Account__r.Thursday_From_2__c,   related_Account__r.Thursday_To_2__c, subject, edit_permission__c, status__c,
									who.name,experties__c, related_Account_Name__c, description, main_Product__c, team__c, secondary_Product__c,
									third_Product__c, target_Priority__c, next_Meeting_Target__c, target__r.experties__c, return_to_Contact__c, location,
									side_Effects__c, side_Effects_Comments__c, side_Effect_Product__c, side_Effect_Dosage__c, side_Effect_Doctor_Consent__c,
									side_Effect__r.call_To_Dr_Consent__c, side_Effect__r.comments__c, side_Effect__r.sE_Dosage__c, side_Effect__r.sE_Product__c,
									side_Effect__r.sE_Way_Of_Use__c
									FROM event
									//   where StartDateTime >: evn.UtilDate__c and  StartDateTime <: thisnextdDay and
									//   WHERE StartDateTime >= :evn.UtilDate__c AND  StartDateTime < :thisnextdDay AND
									WHERE DAY_ONLY(convertTimezone(StartDateTime)) >= :selectedDate
										AND  DAY_ONLY(convertTimezone(StartDateTime)) < :nextdDay
										AND OwnerId = :userinfo.getUserId()
										AND RecordTypeId  = :eventRecordTypeId
										AND (status__c = 'Not started' OR status__c = 'Completed')];
		return eventList;
	}

	private static String getEventTime(Date selectedDate){
		if (selectedDate == Date.today()){
			return 'Today';
		}
		else if (selectedDate < Date.today()) {
			return 'PastDate';	
		} 
		else{
			return 'FutureDate';
		}
	}

	private static String GetThisdayOfWeek(Date dateParam){
		Datetime dt = (DateTime)dateParam;	
		return dt.format('EEEE');
	}

	private static List<WrapperEvent> generateWrappers(List<Event> eventList){
		List<WrapperEvent> wrapperList = new List<WrapperEvent>();	
		for(Event evt : eventList){
			wrapperList.add(new WrapperEvent(evt));
		}
		return wrapperList;
	}

	public class WrapperData{
		@AuraEnabled public List<WrapperEvent> mEventList;
		@AuraEnabled public String mDayOfWeek;

		public WrapperData(){

		}
	}
	public class WrapperEvent{
		@AuraEnabled public Event mEvent;
		@AuraEnabled public Id mEventId;
		@AuraEnabled public Boolean mIsSelected;
		@AuraEnabled public Integer mIndex;

		public WrapperEvent(Event e){
			mEvent = e;
			mEventId = e.Id;
			mIsSelected = false;
		}
	}


}