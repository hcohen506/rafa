public with sharing class Ctrl_ObjectivesClone {

	//public List<Objective__c> objectiveList {get;set;}
	public List<ObjWrapper> ojbWrpLst {get;set;} 
	public String toSelectYear {get; set;}
	public String toInsertYear {get; set;}
	public String toSelectQuarter {get; set;}
	public String toInsertQuarter {get; set;}
	//public String toSelectUser {get; set;}
	//public String toInsertUser {get; set;}
	public Boolean showTable {get; set;}
	public Boolean isTeamlLead {get; set;}
	public String pgSuccessMsg {get; set;}
	public String pgErrorMsg {get; set;}
	public User currentUser {get; set;}

	public class ObjWrapper{
        public Objective__c obj {get;set;}
        public Boolean isSelected {get;set;}

        public ObjWrapper(Objective__c obj , Boolean isSelected ){
            this.obj = obj;
            this.isSelected = isSelected;
        }
    }
	
	private void clearPageMsg(){
		pgSuccessMsg= null;
		pgErrorMsg = null;
	}

	public Ctrl_ObjectivesClone() {
		showTable = false;
		clearPageMsg();
		isTeamlLead = true;
		//List <User> userDetails = [SELECT UserRole.DeveloperName FROM User where Id=:userinfo.getUserId() limit 1 ];
		List <User> userDetails = [SELECT id,name FROM User where Id=:userinfo.getUserId() limit 1 ];
		currentUser = userDetails[0];
		//if (userDetails[0].UserRole.DeveloperName != 'Team_Leader' && 
		//	userDetails[0].UserRole.DeveloperName != 'Team_Leader_2' && 
		//	userDetails[0].UserRole.DeveloperName != 'Team_Leader_3' && 
		//	userDetails[0].UserRole.DeveloperName != 'Department_Manager' )
		//{
		//	pgErrorMsg = 'You are not a Team Leader and unauthorized for this page';	
		//	isTeamlLead = false;		
		//}	
	}


	public List<SelectOption> getYearsFromList(){
		List<SelectOption> yearsList = new List<SelectOption>();		
		yearsList.add( new SelectOption ( String.valueOf(date.today().year()) , String.valueOf(date.today().year()) ) );	
		yearsList.add( new SelectOption ( String.valueOf(date.today().addYears(-1).year()) , String.valueOf(date.today().addYears(-1).year()) ) );	
		return yearsList;
	}

	public List<SelectOption> getYearsToList(){
		List<SelectOption> yearsList = new List<SelectOption>();		
		yearsList.add( new SelectOption ( String.valueOf(date.today().year()) , String.valueOf(date.today().year()) ) );	
		yearsList.add( new SelectOption ( String.valueOf(date.today().addYears(+1).year()) , String.valueOf(date.today().addYears(+1).year()) ) );	
		return yearsList;
	}

	public List<SelectOption> getQrList(){
		List<SelectOption> qrList = new List<SelectOption>();	
		Schema.DescribeFieldResult fieldResult = Objective__c.Quarter__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		for( Schema.PicklistEntry f : ple)
		{
			qrList.add(new SelectOption(f.getLabel(), f.getValue()));
		}  
		return qrList;
	}

	public List<SelectOption> getUsersList(){
		List<SelectOption> usersList = new List<SelectOption>();		
		for( User usr : [select id,name,userroleid from user])
		{
			usersList.add( new SelectOption ( usr.id, usr.name ) );	
		}  
		return usersList;
	}

	public void Search(){
		showTable = false;
		clearPageMsg();
		ojbWrpLst  = new List<ObjWrapper> ();
		//objectiveList = new List<Objective__c> ();
		//List<Objective__c> objectiveList = [select name,Year__c,Quarter__c,Representative__c,Representative__r.name, Classification__c,Experties__c,Objective_Description__c,Required_No_Of_Visits__c,Territory__c
		//				 from Objective__c where Year__c =: toSelectYear and Quarter__c =: toSelectQuarter and Representative__c =: toSelectUser];
		List<Objective__c> objectiveList = [select name,Year__c,Quarter__c,Representative__c,Representative__r.name, Classification__c,Experties__c,Objective_Description__c,Required_No_Of_Visits__c,Territory__c
						 					from Objective__c 
						 					where Year__c =: toSelectYear and Quarter__c =: toSelectQuarter and Representative__c =: currentUser.id];
	 	if (objectiveList != null && !objectiveList.isEmpty() && objectiveList.size()>150)
	 	{
	 		showTable = false;
			pgErrorMsg = 'Too many records. Please filter and search again';	
			pgSuccessMsg = null;
	 	}
	 	else if (objectiveList != null && !objectiveList.isEmpty() && objectiveList.size()>0){
	 		for (Objective__c objective : objectiveList){
	 			ojbWrpLst.add(new ObjWrapper(objective,false)); 	 			
	 		}
	 		showTable = true;
	 	}
	 	else
	 	{
			showTable = false;
			pgErrorMsg = 'No Objectives found';	
			pgSuccessMsg = null;	
	 	}
	}

	public void CloneObjectives(){
		clearPageMsg();
		if (showTable){
			List<Objective__c> objectiveListToInsert = new List<Objective__c>();
			for (ObjWrapper objc : ojbWrpLst) 
			{
				if (objc.isSelected)
                { 
                	//toInsertUser,
					objectiveListToInsert.add( new Objective__c ( Year__c = toInsertYear , Quarter__c = toInsertQuarter , Representative__c = currentUser.id , Classification__c = objc.obj.Classification__c ,
																  Experties__c = objc.obj.Experties__c ,Objective_Description__c = objc.obj.Objective_Description__c ,
																  Required_No_Of_Visits__c = objc.obj.Required_No_Of_Visits__c != null ? objc.obj.Required_No_Of_Visits__c : 0, Territory__c = objc.obj.Territory__c));	
				}
			}
			try{
				if (objectiveListToInsert.size()>0){
					insert objectiveListToInsert;
					pgErrorMsg = null;
					pgSuccessMsg = 'Clone insert successfully';
				}
				else
                {
                    pgSuccessMsg = null;
                    pgErrorMsg = 'Select at least one Objective you want to Clone';
                }
			}
			catch (exception ex){
				system.debug('*** System Exception:' + ex);
				pgSuccessMsg = null;
				pgErrorMsg = 'There was a problem with the Clone process.';
			}
		}
		else{
			pgErrorMsg = 'Search and find Objectives to clone';	
			pgSuccessMsg = null;	
		}
	}
}