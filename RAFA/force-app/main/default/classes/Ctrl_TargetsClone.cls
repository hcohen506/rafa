public with sharing class Ctrl_TargetsClone {
    
    //public List<Target__c> targetList {get;set;}
    public List<targetWrapper> targetWrpList {get;set;}
    public Event objectiveLookUp {get;set;}
    public String toSelectExperties {get; set;}    
    public String toSelectYear {get; set;}
    public String toInsertYear {get; set;}
    public String toSelectQuarter {get; set;}
    public String toInsertQuarter {get; set;}
    public String toSelectUser {get; set;}
    public String toInsertUser {get; set;}
    public Boolean showTable {get; set;}
    //public Boolean isTeamlLead {get; set;}
    public String pgSuccessMsg {get; set;}
    public String pgErrorMsg {get; set;}
    
    private void clearPageMsg(){
        pgSuccessMsg= null;
        pgErrorMsg = null;
    }
    
    public class targetWrapper{
        public Boolean isSelected {get;set;}
        public String territory {get;set;}
        public String classification {get;set;}
        public String experties {get;set;}
        public Decimal requiredNoOfVisits{get;set;}
        //numberOfVisitsPerformed 
        public String accountName {get;set;}
        public String contactName {get;set;}
        public String contactId {get;set;}
        public String objectiveId {get;set;}
        public String objectiveDescription {get;set;}
        //public String product {get;set;}
        public String relatedAccountId {get;set;}

        


        public targetWrapper(Target__c trg , Boolean isSelected ){
            this.isSelected = isSelected;           
            territory = trg.Territory__c;
            classification = trg.Classification__c;
            experties = trg.Experties__c;
            //numberOfVisitsPerformed = trg.Number_Of_Visits_Performed__c;
            accountName = trg.Related_Account__r.account__r.name;
            contactName = trg.Contact__r.name;
            contactId = trg.Contact__c;
            requiredNoOfVisits = trg.Required_No_Of_Visits__c;
            objectiveId = trg.Objective__c;
            objectiveDescription = trg.Objective__r.Objective_Description__c;
          //  product =  trg.Product__c;
            relatedAccountId =  trg.Related_Account__c; 

        }
    }

    public Ctrl_TargetsClone() {
        clearPageMsg ();
        objectiveLookUp = new Event();
        //isTeamlLead = true;
        //List <User> userDetails = [SELECT UserRole.DeveloperName FROM User where Id=:userinfo.getUserId() limit 1 ];
        //if (userDetails[0].UserRole.DeveloperName != 'Team_Leader' && 
        //  userDetails[0].UserRole.DeveloperName != 'Team_Leader_2' && 
        //  userDetails[0].UserRole.DeveloperName != 'Team_Leader_3' )
        //{
        //  pgErrorMsg = 'You are not a Team Leader and unauthorized for this page';    
        //  isTeamlLead = false;        
        //}     
        showTable = false;
        targetWrpList  = new List<targetWrapper> ();    
    }
    
     private List<Schema.PicklistEntry> getPicklistValues(String objName, String fieldName) {
        Schema.SObjectType t = Schema.getGlobalDescribe().get(objName);
        Schema.DescribeSObjectResult r = t.getDescribe();
        Schema.DescribeFieldResult statusFieldDescription = r.fields.getMap().get(fieldName).getDescribe();
        return statusFieldDescription.getPicklistValues();
    }

    public List<SelectOption> populatePickListOption(String obj, String fieldName,String additionalFirstVal){
        List<SelectOption> tempOptions = new List<SelectOption>();
        if (additionalFirstVal != null){
            tempOptions.add(new SelectOption('null', additionalFirstVal));
        }
        for(Schema.PicklistEntry field: getPicklistValues(obj, fieldName)){
            if(field.getValue() != null && field.getLabel() != null){
                tempOptions.add(new SelectOption(field.getValue(), field.getLabel()));
            }
        }           
        return tempOptions;
    }


    public List<SelectOption> getYearsFromList(){
        List<SelectOption> yearsList = new List<SelectOption>();        
        yearsList.add( new SelectOption ( String.valueOf(date.today().year()) , String.valueOf(date.today().year()) ) );    
        yearsList.add( new SelectOption ( String.valueOf(date.today().addYears(-1).year()) , String.valueOf(date.today().addYears(-1).year()) ) );  
        return yearsList;
    }

    public List<SelectOption> getYearsToList(){
        List<SelectOption> yearsList = new List<SelectOption>();        
        yearsList.add( new SelectOption ( String.valueOf(date.today().year()) , String.valueOf(date.today().year()) ) );    
        yearsList.add( new SelectOption ( String.valueOf(date.today().addYears(+1).year()) , String.valueOf(date.today().addYears(+1).year()) ) );  
        return yearsList;
    }
    
     public List<SelectOption> getQrList(){
        return populatePickListOption('Objective__c','Quarter__c', null ); 
    }
    
     public List<SelectOption> getExpertiesList(){
        return populatePickListOption('Target__c','Experties__c', null ); 
    }

   /* public List<SelectOption> getQrList1(){
        List<SelectOption> qrList = new List<SelectOption>();   
        Schema.DescribeFieldResult fieldResult = Objective__c.Quarter__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple)
        {
            qrList.add(new SelectOption(f.getLabel(), f.getValue()));
        }  
        return qrList;
    }*/

    public List<SelectOption> getUsersList(){
        List<SelectOption> usersList = new List<SelectOption>();        
        for( User usr : [select id,name,userroleid from user])
        {
            usersList.add( new SelectOption ( usr.id, usr.name ) ); 
        }  
        return usersList;
    }

    public void Search(){
        showTable = false;
        clearPageMsg();
        targetWrpList  = new List<targetWrapper> ();
        for (Target__c trg : [select id,Objective__c,Objective__r.Objective_Description__c,Related_Account__c,Territory__c, Classification__c, Experties__c, 
                              Required_No_Of_Visits__c,Contact__c, Related_Account__r.account__r.name,Contact__r.name
                              from Target__c 
                              where Experties__c =: toSelectExperties  and  Year__c =: toSelectYear and Quarter__c =: toSelectQuarter and Representative__c =: toSelectUser] )
        {
         //   system.debug('**** TARGET:' +trg.id );
             targetWrpList.add(new targetWrapper(trg,false));
        }   
        if (targetWrpList != null && !targetWrpList.isEmpty() && targetWrpList.size()>150)
        {
            showTable = false;
            pgSuccessMsg = null;
            pgErrorMsg = 'Too many records. Please filter and search again';    
        }
        else if (targetWrpList != null && !targetWrpList.isEmpty() && targetWrpList.size()>0)
            showTable = true;
        else
        {
            showTable = false;
            pgSuccessMsg = null;
            pgErrorMsg = 'No Target found';
        }
    }

    public void CloneTargets(){
        if (objectiveLookUp.Objective__c == null){
            pgSuccessMsg = null;
            pgErrorMsg = 'Please select an Objective to Clone to';  
            return;     
        }
        List <Objective__c> objList = [select Experties__c from Objective__c where id =: objectiveLookUp.Objective__c limit 1];
        //system.debug('*** toSelectExperties : '+toSelectExperties + 'Objective__c-Experties__c: '+objList[0].Experties__c);
        if (toSelectExperties != objList[0].Experties__c){
            pgSuccessMsg = null;
            pgErrorMsg = 'Chosen Experties does not match the Objective Experties ';  
            return;     
        }
        clearPageMsg();
        if (showTable){
            List<Target__c> targetListToInsert = new List<Target__c>();
            for (targetWrapper tr : targetWrpList)
            {
                if (tr.isSelected)
                {
                    targetListToInsert.add ( new Target__c (Objective__c=objectiveLookUp.Objective__c,Year__c = toInsertYear , Quarter__c = toInsertQuarter ,Representative__c = toInsertUser, 
                                                            Classification__c = tr.classification,Related_Account__c = tr.relatedAccountId, 
                                                            Required_No_Of_Visits__c = tr.requiredNoOfVisits, Contact__c = tr.contactId, 
                                                            Experties__c = tr.experties,  Territory__c = tr.territory) );
                }
            }
            try{
                if (targetListToInsert.size()>0)
                {
                    insert targetListToInsert;
                    pgErrorMsg = null;
                    pgSuccessMsg = 'Clone insert successfully';
                    targetWrpList.clear();
                    showTable = false;                  
                }
                else
                {
                    pgSuccessMsg = null;
                    pgErrorMsg = 'Select at least one Target you want to clone to the Objective';
                }
            }
            catch (Exception ex)
            {
                system.debug('*** System Exception:' + ex);
                pgSuccessMsg = null;
                pgErrorMsg = 'There was a problem with the Clone process.';
            }
        }
        else{
            pgErrorMsg = 'Search and find Objectives to clone'; 
            pgSuccessMsg = null;    
        }
    }
}