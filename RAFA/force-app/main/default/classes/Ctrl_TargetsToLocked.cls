public with sharing class Ctrl_TargetsToLocked {

	public String selectYear {get; set;}
	public String selectQuarter {get; set;}
	public String selectRole {get; set;}
	public String roleToQry {get; set;}
	public Boolean isAuthorized {get; set;}
	public Boolean isDepartmentManager {get; set;}
	public String pgSuccessMsg {get; set;}
	public String pgErrorMsg {get; set;}

	public List<SelectOption> getQrList(){
		return populatePickListOption('Target__c','Quarter__c',null);	
	}
	public List<SelectOption> getYearList(){
		return populatePickListOption('Target__c','Year__c',null);	
	}
	public List<SelectOption> getRoleList(){
		List<SelectOption> qrList = new List<SelectOption>();	
		//(NOT name like '%Department%') and (NOT name like '%Representative%')
		for( UserRole ur : [select id,name,DeveloperName  from UserRole where name like '%Leader%'])
		{
			qrList.add(new SelectOption(ur.id, ur.name));
		}  
		//selectRole = !qrList.isEmpty() && qrList.size()>0  ? qrList[0].id : null ;
		//system.debug('qrList: ' + qrList);
		return qrList;
	}

	public Ctrl_TargetsToLocked() {
		clearPageMsg();
		isAuthorized = true;
		isDepartmentManager = false;
		List <User> userDetails = [SELECT UserRoleid,UserRole.Name, UserRole.DeveloperName FROM User where Id=:userinfo.getUserId() limit 1 ];
		//system.debug(' userDetails[0].UserRoleid :' + userDetails[0].UserRoleid );
		if (userDetails[0].UserRoleid == null || userDetails[0].UserRole.Name.containsIgnoreCase('Representative'))
		{
			pgErrorMsg = 'You are unauthorized to perform action for this page';
			pgSuccessMsg = null; 		
			isAuthorized = false;		
		}
		else if	(userDetails[0].UserRole.Name.containsIgnoreCase('Manager')){
			isAuthorized = true;
			isDepartmentManager = true;
		}
		else{
			isAuthorized = true;
			roleToQry = userDetails[0].UserRoleid;
		}
		system.debug ('*** isAuthorized: ' + isAuthorized + ' ;\n isDepartmentManager:' +isDepartmentManager ); 
		//system.debug ('*** selectRole: ' + selectRole); 
		//system.debug ('*** roleToQry: ' + roleToQry); 		
	}
	
	private List<Schema.PicklistEntry> getPicklistValues(String objName, String fieldName) {
		Schema.SObjectType t = Schema.getGlobalDescribe().get(objName);
		Schema.DescribeSObjectResult r = t.getDescribe();
		Schema.DescribeFieldResult statusFieldDescription = r.fields.getMap().get(fieldName).getDescribe();
		return statusFieldDescription.getPicklistValues();
	}

 	public List<SelectOption> populatePickListOption(String obj, String fieldName,String additionalFirstVal){
        List<SelectOption> tempOptions = new List<SelectOption>();
        if (additionalFirstVal != null){
        	tempOptions.add(new SelectOption('null', additionalFirstVal));
        }
        for(Schema.PicklistEntry field: getPicklistValues(obj, fieldName)){
            if(field.getValue() != null && field.getLabel() != null){
                tempOptions.add(new SelectOption(field.getValue(), field.getLabel()));
            }
        }        	
        return tempOptions;
    }

    public void Lock(){
    	if (!isAuthorized){
    		return;
    	}
    	//system.debug ('*** roleToQry1: ' + roleToQry); 
    	//system.debug ('*** selectRole1: ' + selectRole); 
        clearPageMsg();
        if (isDepartmentManager){
        	roleToQry = selectRole;
        }
////system.debug ('*** isAuthorized2: ' + isAuthorized + ' ;\n isDepartmentManager2:' +isDepartmentManager ); 
////system.debug ('*** selectRole2: ' + selectRole); 
//system.debug ('*** roleToQry2: ' + roleToQry); 

    	id targetLockRecordTypeId = Schema.SObjectType.Target__c.getRecordTypeInfosByName().get('Locked Target').getRecordTypeId();        
        List <Target__c> targetListToUpdate = new List<Target__c>();
        Set <id> usersIdSet = new Set<id>();
        List <UserRole> usrRlLst = new List <UserRole>([Select Id, DeveloperName, ParentRoleId,(select id from users) from UserRole where ParentRoleId =: roleToQry]);         
        for (UserRole usrl :  usrRlLst){
        	for (User usr :usrl.users ){
        		usersIdSet.add (usr.id);      	        		
        	}
       }
      //system.debug ('*** usersIdSet: ' + usersIdSet ); 
      
        for (Target__c trg : [select id,recordtypeid from Target__c 
        					  where recordtypeid !=: targetLockRecordTypeId and  Year__c =: selectYear and Quarter__c =: selectQuarter and Representative__c in: usersIdSet] )
		{
			targetListToUpdate.add (new Target__c (id = trg.id, recordtypeid = targetLockRecordTypeId  ));
		}
		try{
			if (!targetListToUpdate.isEmpty() && targetListToUpdate.size()>0){
				system.debug('*** targetListToUpdate: ' + targetListToUpdate);
				update targetListToUpdate;	
				pgSuccessMsg = 'Target Lock processed successfully';  
            	pgErrorMsg =  null;  		
			}
			else{
				pgErrorMsg = 'No Targets found for Lock process'; 
				pgSuccessMsg = null; 	
			}

		}
		catch(DmlException e) {
			system.debug('Error: ' + e);
			pgSuccessMsg = null;
			//' \n in field:' + e.getDmlFieldNames(0) + 
            pgErrorMsg = 'There was a problem with Update Process in id : ' +  e.getDmlId(0) +  ' \n  The Error Messages is: ' + e.getDmlMessage(0);   

		}		
		//	catch (Exception ex){
		//	system.debug('Error: ' + ex);
		//	pgSuccessMsg = null;
		//  pgErrorMsg = 'There was a problem with Target Lock process: ' + ex.getMessage();   
		//}
    }

    private void clearPageMsg(){
		pgSuccessMsg= null;
		pgErrorMsg = null;
	}
}