public with sharing class Ctrl_addParticipants {
    public String pgSuccessMsg {get; set;}
    public String pgErrorMsg {get; set;}
    public String noParticipantMsg {get; set;}
    public Boolean showConfParticipantTable {get; set;}
    public Boolean showTable {get; set;} // AC_Relationship Table
    public String conferenceId {get; set;}
    public List<Conf_Participant__c> confParticipantList {get;set;}
    public List<Conference__c> thisConference {get;set;}
    public List<AccConWrapper> accConWrpLst {get;set;} 

    public AC_Relationship__c accLookUp {get;set;}
    public List<String> toSelectInterests {get; set;} 
    public String toSelectExpertise {get; set;}
    //public String toSelectOwnership {get; set;}
    public String toSelectBelongsto {get; set;}
    public String toSelectClassification {get; set;}

    public List<SelectOption> getInterestsList(){       
        return populatePickListOption('Contact','Interests__c',null);   
    }
    public List<SelectOption> getClassificationList(){
        return populatePickListOption('AC_Relationship__c','Classification__c','All');   
    }
    public List<SelectOption> getExpertiesList(){
        return populatePickListOption('AC_Relationship__c','Expertise__c','All'); 
    }
    //public List<SelectOption> getOwnershipList(){
    //    return populatePickListOption('Account','Ownership','All'); 
    //}
    public List<SelectOption> getBelongstoList(){
        return populatePickListOption('Account','Belongs_to__c','All'); 
    }

    public void Create_confParticipantList(){
        confParticipantList = new List<Conf_Participant__c>([select Conference__c,Conference__r.name,Contact__c,Full_Name__c,Contact__r.KOL__c,Expertise__c
                                                             from Conf_Participant__c 
                                                             where Conference__c =: conferenceId and  Contact__r.Status__c = 'Active']);
        if (confParticipantList != null && !confParticipantList.isEmpty() && confParticipantList.size()>0){
            showConfParticipantTable = true;            
        }
        else 
        {
            noParticipantMsg = 'You have no Participant in this Conference';
            showConfParticipantTable = false;
        }
    }

    public Ctrl_addParticipants() {
        showTable = false;
        ClearPageMsg();
        accLookUp = new AC_Relationship__c();
        noParticipantMsg = null;
        conferenceId = Apexpages.currentPage().getParameters().get('conferenceId');
        thisConference = [select id,name from Conference__c where id =: conferenceId limit 1] ;
        Create_confParticipantList();
        
        //confParticipantList = new List<Conf_Participant__c>([select Conference__c,Conference__r.name,Contact__c,Full_Name__c,Contact__r.KOL__c,Expertise__c
        //                                                     from Conf_Participant__c 
        //                                                     where Conference__c =: conferenceId and  Contact__r.Status__c = 'Active']);
        //if (confParticipantList != null && !confParticipantList.isEmpty() && confParticipantList.size()>0){
        //    showConfParticipantTable = true;            
        //}
        //else 
        //{
        //    noParticipantMsg = 'You have no Participant in this Conference';
        //    showConfParticipantTable = false;
        //}
    }

      //public class  ConfPartWrpr{
      //  public Conf_Participant__c confPart {get;set;}
      //  public Boolean isSelected {get;set;}

      //  public ConfPartWrpr(Conf_Participant__c confPart , Boolean isSelected ){ 
      //      this.confPart = confPart;
      //      this.isSelected = isSelected;
      //  }

        public class AccConWrapper{
        public AC_Relationship__c accnt {get;set;}
        public Boolean isSelected {get;set;}
       

        public AccConWrapper(AC_Relationship__c ac , Boolean isSelected ){
            this.accnt = ac;
            this.isSelected = isSelected;
        }
    }

    //public void AddConfParticipant (){
    //    List<Conf_Participant__c> toAddConfParList = new List<Conf_Participant__c>();
        
    //    try{
    //        insert toAddConfParList;
    //    }
    //    catch(exception ex){
    //        system.debug('exception: ' + ex);
    //    }
    //}


    public void  Search(){
        showTable = false;
        clearPageMsg();
        accConWrpLst  = new List<AccConWrapper> ();
        boolean isFiltered = false;
        String qryString = 'Select Contact__r.name,Contact__c,Account__c,Full_Name__c,Account__r.name,Expertise__c,Classification__c from AC_Relationship__c ';
        String whereString = ' where Status__c != \'Closed\' ' ;
       // system.debug('*** toSelectInterests: ' + toSelectInterests + ' ;accLookUp.util_toAccount__c: ' + accLookUp.util_toAccount__c + ' ;toSelectClassification: ' + toSelectClassification + ' ;toSelectExpertise: ' + toSelectExpertise+ ' ;toSelectBelongsto: '+toSelectBelongsto);
        if (toSelectBelongsto != 'null' && toSelectBelongsto != null ){
            whereString += ' and Account__r.Belongs_to__c = \'' + toSelectBelongsto +'\'' ;           
        }
        if (toSelectExpertise != 'null' && toSelectExpertise != null){
            whereString += ' and Expertise__c = \'' + toSelectExpertise +'\'' ;           
            // whereString += whereString !=null ? ' and Expertise__c = \'' + toSelectExpertise +'\'' :  ' where Expertise__c = \'' + toSelectExpertise +'\'' ;           
        }
        if (toSelectClassification != 'null' && toSelectClassification != null){
            whereString +=  ' and Classification__c = \'' + toSelectClassification +'\'' ;           
           // whereString += whereString !=null ? ' and Classification__c = \'' + toSelectClassification +'\'' :  ' where Classification__c = \'' + toSelectClassification +'\'' ;           
        }
        if (accLookUp.util_toAccount__c != null){
            whereString += ' and Account__c = \''+ accLookUp.util_toAccount__c +'\'' ;           
            //whereString += whereString !=null ? ' and Account__c = \''+ accLookUp.util_toAccount__c +'\'' :  ' where Account__c = \'' + accLookUp.util_toAccount__c +'\'' ;           
        }
         if (toSelectInterests != null && !toSelectInterests.isEmpty() && toSelectInterests.size()>0 )
        {
            //System.debug('*** toSelectInterests: ' + toSelectInterests);
            String newStr = '' ;
            for (string  intr : toSelectInterests){
                newStr += '\'' + intr.trim() + '\',';
            }
            newStr = newStr.Substring(0,newStr.length()-1); 
            //IN
            whereString += ' and Contact__r.Interests__c  includes  (' + newStr + ')' ;      
            //whereString += whereString !=null ? ' and Contact__r.Interests__c  includes  (' + newStr + ')'  :  ' where Contact__r.Interests__c  includes (' + newStr + ')' ;      
            //qryString += ' and Contact__r.Interests__c  IN (' + newStr + ')';                
            //System.debug('*** newStr : ' + newStr);
        }
        // ' and Account__c = \''+ accLookUp.util_toAccount__c +'\'' ;

        /*if (whereString !=null){
            whereString= whereString.removeStart('null');
            qryString += whereString;
        }*/
        qryString += whereString;
        system.debug('qryString: ' + qryString);

        for (AC_Relationship__c acct : Database.query(qryString))
        {
             accConWrpLst.add(new AccConWrapper(acct,false)); 
        }
        if (accConWrpLst != null && !accConWrpLst.isEmpty() && accConWrpLst.size()>200)
        {
            showTable = false;
            pgSuccessMsg = null;
            pgErrorMsg = 'Too many records. Please filter and search again';    
        }
        else if (accConWrpLst != null && !accConWrpLst.isEmpty() && accConWrpLst.size()>0)
            showTable = true;
        else
        {
            showTable = false;
            pgSuccessMsg = null;
            pgErrorMsg = 'No members found in your search';
        }
    }

    public void addParticipants(){
        clearPageMsg();
        if (showTable){
            List<Conf_Participant__c> confPartToInsert = new List<Conf_Participant__c>();
            set <id> setAccCon = new set <id>();
            //System.debug('accConWrpLst: ' + accConWrpLst);
            //System.debug('1: ' + accConWrpLst[0].accnt.contact__c);
            for (AccConWrapper accCon : accConWrpLst)
            {
                system.debug('*** accCon.isSelected: '+ accCon.isSelected + ' ;setAccCon:  ' + setAccCon + ' ;setAccCon.contains:' + setAccCon.contains(accCon.accnt.contact__c) );
                if (accCon.isSelected &&  !setAccCon.contains(accCon.accnt.contact__c))
                {
                    confPartToInsert.add(new Conf_Participant__c (Contact__c = accCon.accnt.contact__c, Conference__c = thisConference[0].id ));
                    setAccCon.add(accCon.accnt.contact__c);
                }
            }
            try{
                if (confPartToInsert.size()>0)
                {
                    insert confPartToInsert;
                    pgErrorMsg = null;
                    pgSuccessMsg = 'Added members successfully to the Conference';
                    accConWrpLst.clear();
                    showTable = false; 
                    Create_confParticipantList();                 
                }
                else
                {
                    pgSuccessMsg = null;
                    pgErrorMsg = 'Select at least one members you want to add to the Conference';
                }
            }
            catch (Exception ex)
            {
                system.debug('*** System Exception:' + ex);
                pgSuccessMsg = null;
                pgErrorMsg = 'There was a problem with the process.';
            }
        }
        else{
            pgErrorMsg = 'Search and find AC Relationship members to add to the Conference'; 
            pgSuccessMsg = null;    
        }
    }

    private void ClearPageMsg(){
        system.debug('### IN ClearPageMsg');
        pgSuccessMsg= null;
        pgErrorMsg = null;
    }
    private List<Schema.PicklistEntry> getPicklistValues(String objName, String fieldName) {
        Schema.SObjectType t = Schema.getGlobalDescribe().get(objName);
        Schema.DescribeSObjectResult r = t.getDescribe();
        Schema.DescribeFieldResult statusFieldDescription = r.fields.getMap().get(fieldName).getDescribe();
        return statusFieldDescription.getPicklistValues();
    }
    public List<SelectOption> populatePickListOption(String obj, String fieldName,String additionalFirstVal){
        List<SelectOption> tempOptions = new List<SelectOption>();
        if (additionalFirstVal != null){
            tempOptions.add(new SelectOption('null', additionalFirstVal));
        }
        for(Schema.PicklistEntry field: getPicklistValues(obj, fieldName)){
            if(field.getValue() != null && field.getLabel() != null){
                tempOptions.add(new SelectOption(field.getValue(), field.getLabel()));
            }
        }           
        return tempOptions;
    }
}