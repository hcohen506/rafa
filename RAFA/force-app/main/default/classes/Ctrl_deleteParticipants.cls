public with sharing class Ctrl_deleteParticipants {
    
    public String pgSuccessMsg {get; set;}
    public String pgErrorMsg {get; set;}
    public String noParticipantMsg {get; set;}
    public String noConfParticipantMsg {get; set;}
    public Boolean showConfParticipantTable {get; set;}
    public String conferenceId {get; set;}
    public List<ConfPartWrpr> confPartWrprList {get;set;}
    public List<Conference__c> thisConference {get;set;}
    //public List<Conf_Participant__c> confParticipantList {get;set;}

    public Ctrl_deleteParticipants() {
        //confPartWrprList  = new List<ConfPartWrpr> ();
        noParticipantMsg = null;
        pgErrorMsg = null;
        pgSuccessMsg = null;
        conferenceId = Apexpages.currentPage().getParameters().get('conferenceId');
        thisConference = [select name from Conference__c where id =: conferenceId limit 1] ;
        Create_confPartWrprList();
        //for (Conf_Participant__c cnfPr : [select id,Conference__c,Conference__r.name,Contact__c,Full_Name__c,Contact__r.KOL__c,Expertise__c
        //                                                     from Conf_Participant__c 
        //                                                     where Conference__c =: conferenceId and  Contact__r.Status__c = 'Active'])
        //{
        //     confPartWrprList.add(new ConfPartWrpr(cnfPr,false)); 
        //}

        //confParticipantList = new List<Conf_Participant__c>([select Conference__c,Conference__r.name,Contact__c,Full_Name__c,Contact__r.KOL__c,Expertise__c
        //                                                     from Conf_Participant__c 
        //                                                     where Conference__c =: conferenceId and  Contact__r.Status__c = 'Active']);
        if (confPartWrprList != null && !confPartWrprList.isEmpty() && confPartWrprList.size()>0){
            showConfParticipantTable = true;            
        }
        else 
        {
            noParticipantMsg = 'You have no Participant in this Conference';
            showConfParticipantTable = false;
        }           
    }

    public class  ConfPartWrpr{
        public Conf_Participant__c confPart {get;set;}
        public Boolean isSelected {get;set;}

        public ConfPartWrpr(Conf_Participant__c confPart , Boolean isSelected ){ 
            this.confPart = confPart;
            this.isSelected = isSelected;
        }
    }

     public void DeleteConfParticipant (){
        List <Conf_Participant__c> toDeleteSet = new List<Conf_Participant__c>();
        for (ConfPartWrpr cnfPrt : confPartWrprList)
            {
                if (cnfPrt.isSelected)
                {
                    toDeleteSet.add(cnfPrt.confPart);                    
                }
            }
        
        try{
			if (toDeleteSet != null && !toDeleteSet.isEmpty() && toDeleteSet.size()>0){
				delete toDeleteSet;	
				Create_confPartWrprList();			
				pgSuccessMsg = 'Participants were deleted successfully';
	    		pgErrorMsg = null;      
	    		//showConfParticipantTable = false;      							
			}
			else{
				pgErrorMsg = 'Select a participant to delete';
        		pgSuccessMsg = null;   
			}
        }
        catch(exception ex){
            system.debug('exception: ' + ex);
        }
    }

    public void Create_confPartWrprList(){
    	confPartWrprList  = new List<ConfPartWrpr> ();
    	for (Conf_Participant__c cnfPr : [select id,Conference__c,Conference__r.name,Contact__c,Full_Name__c,Contact__r.KOL__c,Expertise__c
                                                             from Conf_Participant__c 
                                                             where Conference__c =: conferenceId and  Contact__r.Status__c = 'Active'])
        {
             confPartWrprList.add(new ConfPartWrpr(cnfPr,false)); 
        }

    }

}