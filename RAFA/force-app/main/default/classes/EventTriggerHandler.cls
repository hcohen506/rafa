public with sharing class EventTriggerHandler {
	public void UpdateClassification(List<Event> newList,Map<Id,Event> oldMap, Map<Id,Event> newMap){
		String target_prefix = Schema.SObjectType.Target__c.getKeyPrefix();
		id eventRecordTypeId = Schema.SObjectType.Event.getRecordTypeInfosByName().get('Meeting').getRecordTypeId();  
		set <id> eventIdSet = new set <id>();
		set <id> targetIdSet = new set <id>();
		for (Event e : newList) {
			if(e.RecordTypeId.equals(eventRecordTypeId) &&  string.valueof(e.WhatId).startsWith(target_prefix) 
			   && e.Related_Account__c !=null && e.Status__c == 'Completed' && e.Classification__c != null  &&
			 	  (Trigger.isInsert ) ||
				  (Trigger.isUpdate && 
			  	           (
			  	           	(oldMap.get(e.id).Classification__c != newMap.get(e.id).Classification__c) || 
			  		        (oldMap.get(e.id).Status__c != newMap.get(e.id).Status__c )
			  		       )
			  	   )
			   )
			{
				eventIdSet.add(e.id);
			}
		}
		//list <Target__c> targetToUpdate = new list <Target__c>();
		list <AC_Relationship__c> aCRelToUpdate = new list <AC_Relationship__c>();
		id relatedAcc = null;
		for (Event e : [ select Classification__c,WhatId,Related_Account__c from Event  where id in: eventIdSet]){
			if (e.Classification__c=='I1' || e.Classification__c=='I2' || e.Classification__c=='R' || e.Classification__c=='RA'){
				//targetToUpdate.add(new Target__c (id= e.whatid, Classification__c = e.Classification__c));
				aCRelToUpdate.add(new AC_Relationship__c (id= e.Related_Account__c, Classification__c = e.Classification__c));
			}
			else if (e.Classification__c=='A' || e.Classification__c=='B' || e.Classification__c=='C' || e.Classification__c=='D')
			{
				//system.debug('*** e.WhatId: ' + e.WhatId);
				targetIdSet.add(e.WhatId);
				if (relatedAcc == null)
					relatedAcc = e.Related_Account__c;
			}

		}
		list <Target__c> targetToCheck = new list <Target__c>();
		//SELECT id, (SELECT id FROM events) FROM Target__c
		targetToCheck = [ select id,
									 (select Util_ClassificationCalc__c,Related_Account__c from  events 
						  		      where Util_ClassificationCalc__c != 0 AND Util_ClassificationCalc__c != null AND Status__c = 'Completed' AND  RecordTypeId =: eventRecordTypeId 
						  		      		AND (Classification__c = 'A' OR Classification__c = 'B' OR  Classification__c = 'C' OR Classification__c = 'D' )
						  		      order by StartDateTime desc  limit 6  ) 
						  from Target__c where id in: targetIdSet];

	    //system.debug('*** targetToCheck: ' + targetToCheck);
		for (Target__c trg : targetToCheck ){
			decimal total  = 0;
			decimal cntNum = trg.events != null &&  trg.events.size()>0 ? trg.events.size() : null ;
			decimal avrNum = 0;
			//id relatedAcc = null;
			if (cntNum != null){
				for ( Event ev : trg.events){
					total  += ev.Util_ClassificationCalc__c;
					//if (relatedAcc == null)
					//	relatedAcc = ev.Related_Account__c;
				}				
				avrNum = (total/cntNum).round(System.RoundingMode.HALF_UP);
				string newClassification;
				if ( avrNum == 4)
					newClassification = 'A';
				else if ( avrNum == 3)
					newClassification = 'B';
				else if ( avrNum == 2)
					newClassification = 'C';
				else if ( avrNum == 1)
					newClassification = 'D';							
				//targetToUpdate.add(new Target__c (id= trg.id, Classification__c = newClassification));
				aCRelToUpdate.add(new AC_Relationship__c (id= relatedAcc, Classification__c = newClassification));
			}			
		}
		//system.debug('*** aCRelToUpdate: ' + aCRelToUpdate);
		if (!aCRelToUpdate.IsEmpty() && aCRelToUpdate.size()>0){
			try{
				//update targetToUpdate;
				update aCRelToUpdate;
			}
			catch (Exception ex) {
				system.debug('*** system exception : '  + ex);
			}			
		}
	}
}