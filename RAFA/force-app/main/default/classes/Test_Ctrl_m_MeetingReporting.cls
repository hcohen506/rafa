@isTest
public with sharing class Test_Ctrl_m_MeetingReporting {
    public Test_Ctrl_m_MeetingReporting() {

    }

    @TestSetup
    static void makeData(){
        //Data creator object
        Cls_ObjectCreator cls = new Cls_ObjectCreator();
        //objectives
        Objective__c obj = cls.createObjective(UserInfo.getUserId());
        //account
        Account acc = cls.CreateParentAccount();
        Account acc2 = new Account(Name = 'acc');
        insert acc2;
        //contact
        Contact con = cls.createContact(acc2);
        //ac relationship
        AC_Relationship__c acr = cls.createAC_Relationship(acc2, con);
        //event related to the first inserted contact
       // Event ev = cls.createEvent(con.Id);
       //targets
       List<Target__c> targets = new List<Target__c>();
       targets.add(cls.returnTarget());
       targets.add(cls.returnTarget());
       targets.add(cls.returnTarget());
       targets.add(cls.returnTarget());
       targets.add(cls.returnTarget());
       targets.add(cls.returnTarget());       
       for(Target__c t: targets){
           t.Contact__c = con.Id;
           t.Related_Account__c = acr.Id;
       }
       try{
            insert targets;
       }catch(Exception e){
           system.debug(e.getStackTraceString());
       }
    }

    @isTest
    public static void test1SetContactParam(){
        //ctrl instance
        Ctrl_m_MeetingReporting ctrl = new Ctrl_m_MeetingReporting();
        ctrl.selectedTargetsMap = new Map <Id,Target__c>();
        ctrl.selectedContacts = new Set<Id>();
        ctrl.newEvent = new Event();
        ctrl.setContactParam();
    }

    @isTest
    public static void test2SetContactParam(){ //selected contact = '000000000000000000'
        //ctrl instance
        Ctrl_m_MeetingReporting ctrl = new Ctrl_m_MeetingReporting();
        ctrl.selectedcontact = '000000000000000000';
        ctrl.setContactParam();
    }

    @isTest
    public static void test3SetContactParam(){ //selected contacts contain the selected contact
        //ctrl instance
        Ctrl_m_MeetingReporting ctrl = new Ctrl_m_MeetingReporting();
        ctrl.selectedTargetsMap = new Map <Id,Target__c>();
        ctrl.selectedcontact = '100000000000000000';
        Set<Id> s = new Set<Id>();
        s.add(ctrl.selectedcontact);
        ctrl.selectedContacts = s;
        ctrl.setContactParam();
    }

    @isTest
    public static void test4SetContactParam(){
        //ctrl instance
        Ctrl_m_MeetingReporting ctrl = new Ctrl_m_MeetingReporting();
        ctrl.selectedTargetsMap = new Map <Id,Target__c>();
        ctrl.selectedContacts = new Set<Id>();
        ctrl.newEvent = new Event();
        ctrl.todayEvnt = new List<Event>();
        ctrl.todayEvnt.add(new Event(Location = 'gggggg'));
        ctrl.setContactParam();
    }

    @isTest
    public static void test5SetContactParam(){
        //ctrl instance
        Ctrl_m_MeetingReporting ctrl = new Ctrl_m_MeetingReporting();
        ctrl.selectedTargetsMap = new Map <Id,Target__c>();
        ctrl.selectedContacts = new Set<Id>();
        ctrl.newEvent = new Event();
        ctrl.targetToBeSelectedMap = new Map<Id,Target__c>([select Id From Target__c]);
        ctrl.eventTime = 'Today';
        ctrl.setContactParam();
    }

    @isTest
    public static void testObjctvDetailsList(){
        //ctrl instance
        Ctrl_m_MeetingReporting ctrl = new Ctrl_m_MeetingReporting();
        //for objective to be queried
        ctrl.thisSelectedQuarter = '1';
        ctrl.evn.UtilDate__c = Date.today();
        Objective__c obj = [select quarter__c, year__c From Objective__c Limit 1];
        obj.Quarter__c = ctrl.thisSelectedQuarter;
        obj.Year__c = string.valueof(ctrl.evn.UtilDate__c.year());
        update obj;
        ctrl.ObjctvDetailsList();
    }

    @isTest
    public static void testAccountConList(){
        //ctrl instance
        Ctrl_m_MeetingReporting ctrl = new Ctrl_m_MeetingReporting();
        //selected contact equals the ac relationships
        Contact con = [select Id From Contact Limit 1];
        ctrl.selectedcontact = con.Id;
        //updating status to aloow the query
        AC_Relationship__c acr = [select Status__c from AC_Relationship__c Limit 1];
        acr.Status__c = 'Active';
        update acr;
        ctrl.AccountConList();
    }

    //both return contact 1&2 are unable to query events because pof insertion error of event

    @isTest
    public static void test1ReturntoContact(){ //correct values for the query
        //ctrl instance
        Ctrl_m_MeetingReporting ctrl = new Ctrl_m_MeetingReporting();
        Contact con = [select Id From Contact Limit 1];
        ctrl.selectedcontact = con.Id; //event who id is the same, the code in setup made the assigning
        ctrl.returnToContact();
    }

    @isTest
    public static void testReturntoContact2(){ //incorrect values for the query -> null
        //ctrl instance
        Ctrl_m_MeetingReporting ctrl = new Ctrl_m_MeetingReporting();
        ctrl.returnToContact();
    }

    @isTest
    public static void test1ContactWithMeeting(){ // is to continue is yes
        //ctrl instance
        Ctrl_m_MeetingReporting ctrl = new Ctrl_m_MeetingReporting();
        ctrl.isToContinue = 'Yes';
        ctrl.todayEvnt = new List<Event>();
        ctrl.selectedTargetsMap = new Map <Id,Target__c>();
        ctrl.selectedContacts = new Set<Id>();
        ctrl.newEvent = new Event();
        ctrl.contactWithMeeting();
    }

    @isTest
    public static void test2ContactWithMeeting(){ // is to continue is not yes
        //ctrl instance
        Ctrl_m_MeetingReporting ctrl = new Ctrl_m_MeetingReporting();
        ctrl.isToContinue = '';
        ctrl.contactWithMeeting();
    }

    
    @isTest
    public static void test1AddTargToMap(){ // targets map size > 5
        //ctrl instance
        Ctrl_m_MeetingReporting ctrl = new Ctrl_m_MeetingReporting();
       
        ctrl.selectedTargetsMap = new Map<Id,Target__c>([Select Id From Target__c]);
        ctrl.addTrgToMap();
    }

    @isTest
    public static void test2AddTargToMap(){ // targets map size < 5 && not equals 1
        //ctrl instance
        Ctrl_m_MeetingReporting ctrl = new Ctrl_m_MeetingReporting();
        
        ctrl.selectedTargetsMap = new Map<Id,Target__c>([Select Id From Target__c Order By Id ASC LIMIT 4]);
        ctrl.targetToBeSelectedMap = ctrl.selectedTargetsMap;
        ctrl.selectedtrgId = [Select Id From Target__c Order By Id ASC LIMIT 1].Id;
        ctrl.newEvent = new Event();
        ctrl.newEvent.Objective__c = null;
        ctrl.addTrgToMap();
    }

    @isTest
    public static void test3AddTargToMap(){ // targets map size is 1
        //ctrl instance
        Ctrl_m_MeetingReporting ctrl = new Ctrl_m_MeetingReporting();
        
        Target__c tempTar = [Select Id, Classification__c, Experties__c From Target__c Order By Id ASC LIMIT 1];
        tempTar.Classification__c = 'A';
        tempTar.Experties__c = 'PCR';
        update tempTar;
        ctrl.selectedTargetsMap = new Map<Id,Target__c>([Select Id, Classification__c, Experties__c From Target__c Order By Id ASC LIMIT 1]);
        ctrl.targetToBeSelectedMap = ctrl.selectedTargetsMap;
        ctrl.selectedtrgId = tempTar.Id;
        ctrl.newEvent = new Event();
        ctrl.newEvent.Objective__c = null;
        ctrl.addTrgToMap();
    }

    @isTest
    public static void test1RemoveTarg(){ // remove id == newevent.objective__c
        //ctrl instance
        Ctrl_m_MeetingReporting ctrl = new Ctrl_m_MeetingReporting();
        Id tId = [Select Id From Target__c Order By Id ASC LIMIT 1].Id;
        ctrl.removedId = tId;
        ctrl.newEvent = new Event();
        ctrl.newEvent.Objective__c = tId;
        ctrl.selectedTargetsMap = new Map<Id,Target__c>();
        ctrl.RemoveTrg();
    }

    @isTest
    public static void test2RemoveTarg(){ // remove id != newevent.objective__c ->else
        //ctrl instance
        Ctrl_m_MeetingReporting ctrl = new Ctrl_m_MeetingReporting();
        Target__c t = [Select Id From Target__c Order By Id ASC LIMIT 1];
        Contact con = [select Id From Contact Limit 1];
        ctrl.selectedContacts = new Set<Id>();
        ctrl.selectedContacts.add(con.Id);
        t.Contact__c = con.Id;
        update t;
        ctrl.removedId = t.Id;
        System.debug('{{{{'+ t.Id);
        ctrl.newEvent = new Event();
        ctrl.newEvent.Objective__c = null;
        ctrl.selectedTargetsMap = new Map<Id,Target__c>([Select Id, Contact__c From Target__c Order By Id ASC LIMIT 1]);
        ctrl.RemoveTrg();
    }

    @isTest
    public static void testGetItems1(){
        //ctrl instance
        Ctrl_m_MeetingReporting ctrl = new Ctrl_m_MeetingReporting();
      //  ctrl.getItems1();
    }

    @isTest
    public static void test1SaveEvents(){//selectedcontact=='000000000000000000'
        //ctrl instance
        Ctrl_m_MeetingReporting ctrl = new Ctrl_m_MeetingReporting();
        ctrl.selectedcontact = '000000000000000000';
        ctrl.SaveEvents();
    }

    @isTest
    public static void test2SaveEvents(){//selectedcontact is queried, incorrect start time-> return;
        //ctrl instance
        Ctrl_m_MeetingReporting ctrl = new Ctrl_m_MeetingReporting();
        ctrl.selectedcontact = [select Id From Contact Limit 1].Id;
        ctrl.startsTime = 'hhhhhhhh';
        ctrl.SaveEvents();
    }

    @isTest
    public static void test3SaveEvents(){//new event is null-> exception
        //ctrl instance
        Ctrl_m_MeetingReporting ctrl = new Ctrl_m_MeetingReporting();
        ctrl.selectedcontact = [select Id From Contact Limit 1].Id;
        ctrl.SaveEvents();
    }

    @isTest
    public static void test4SaveEvents(){//'You didn\'t select a Target or an Objective for Creating a Meeting' -> return
        //ctrl instance
        Ctrl_m_MeetingReporting ctrl = new Ctrl_m_MeetingReporting();
        ctrl.selectedcontact = [select Id From Contact Limit 1].Id;
        ctrl.newEvent = new Event();
        ctrl.startsTime = '22:22';
        ctrl.endsTime = '23:23';
        ctrl.selectedTargetsMap = new Map<Id, Target__c>();
        ctrl.SaveEvents();
    }

    @isTest
    public static void test5SaveEvents(){//getting all error msgs from the input inner method call
        //ctrl instance
        Ctrl_m_MeetingReporting ctrl = new Ctrl_m_MeetingReporting();
        ctrl.selectedcontact = [select Id From Contact Limit 1].Id;
        ctrl.newEvent = new Event();
        ctrl.newEvent.Related_Account__c = [Select Id From Account LIMIT 1].Id;
        ctrl.newEvent.Objective__c = [Select Id From Objective__c LIMIT 1].Id;
        ctrl.selectedConIdForObjective = [select Id From Contact Limit 1].Id;
        ctrl.startsTime = '22:22';
        ctrl.endsTime = '23:23';
        ctrl.selectedTargetsMap = new Map<Id, Target__c>([Select Id, Contact__c From Target__c Order By Id ASC LIMIT 1]);
        Ctrl.nextEvn = new Event();
        Ctrl.nextEvn.UtilDate__c = system.today();
        ctrl.eventTime = 'Today';
        ctrl.thisEvent = new Event();
        //ctrl.thisEvent.side_Effects__c = true;
        ctrl.thisSideEffects = new Side_Effects__c();
        ctrl.thisSideEffects.comments__c = null;
        ctrl.thisEvent.utilEndTime__c = Time.newInstance(1,2,3,3);
        ctrl.thisEvent.UtilStartTime__c = ctrl.thisEvent.utilEndTime__c.addHours(1);
        ctrl.newEvent.side_Effects__c = true;
        //ctrl.newEvent.utilEndTime__c = Time.newInstance(1,2,3,3);
        //ctrl.newEvent.utilStartTime__c = ctrl.newEvent.utilEndTime__c.addHours(1);
        ctrl.nextEvn.UtilDate__c = System.today().addDays(-1);
        ctrl.SaveEvents();
    }

    @isTest
    public static void test6SaveEvents(){//return true from helper method && exception - related account null
        //ctrl instance
        Ctrl_m_MeetingReporting ctrl = new Ctrl_m_MeetingReporting();
        ctrl.selectedcontact = [select Id From Contact Limit 1].Id;
        ctrl.newEvent = new Event();
        ctrl.newEvent.Related_Account__c = [Select Id From Account LIMIT 1].Id;
        ctrl.newEvent.Objective__c = [Select Id From Objective__c LIMIT 1].Id;
        ctrl.selectedConIdForObjective = [select Id From Contact Limit 1].Id;
        ctrl.startsTime = '22:22';
        ctrl.endsTime = '23:23';
        ctrl.selectedTargetsMap = new Map<Id, Target__c>([Select Id, Contact__c, Related_Account__c From Target__c Order By Id ASC LIMIT 1]);
        Ctrl.nextEvn = new Event();
        ctrl.thisEvent = null;
        ctrl.newEvent = new Event();
        ctrl.newEvent.Description = 'my supreme description';
        ctrl.newEvent.classification__c = 'A';
        ctrl.selectedMainProduct = 'Product';
        Ctrl.nextEvn.UtilDate__c = System.today();
        ctrl.eventTime = '';
        ctrl.newEvent.Side_Effects__c = true;
        ctrl.newEvent.related_Account__c = [Select Id From Account LIMIT 1].Id;
        ctrl.newEvent.objective__c = [Select Id From Objective__c LIMIT 1].Id;
        ctrl.selectedConIdForObjective = [Select Id From Contact LIMIT 1].Id;
        ctrl.thisSideEffects = new Side_Effects__c();
        ctrl.thisSideEffects.Comments__c = 'HALELUIA';
        ctrl.thisSideEffects.sE_Product__c = 'EPIDUO';
        ctrl.thisSideEffects.sE_Way_Of_Use__c = 'Whatever yore doing, make a reverse';
        ctrl.thisSideEffects.sE_Dosage__c = 'The one and only';
        ctrl.thisSideEffects.call_To_Dr_Consent__c = true;
        //updating related account setted to null to cause exception
        List<Target__c> targets = [Select Id,Related_Account__c From Target__c];
        for(Target__c t: targets){
           t.Related_Account__c = null;
       }
       update targets;
       ctrl.SaveEvents();
    }

    @isTest
    public static void test7SaveEvents(){//other errors
        //ctrl instance
        Ctrl_m_MeetingReporting ctrl = new Ctrl_m_MeetingReporting();
        ctrl.selectedcontact = [select Id From Contact Limit 1].Id;
        ctrl.newEvent = new Event();
        ctrl.newEvent.Related_Account__c = [Select Id From Account LIMIT 1].Id;
        ctrl.newEvent.Objective__c = [Select Id From Objective__c LIMIT 1].Id;
        ctrl.selectedConIdForObjective = [select Id From Contact Limit 1].Id;
        ctrl.startsTime = '22:22';
        ctrl.endsTime = '23:23';
        ctrl.selectedTargetsMap = new Map<Id, Target__c>([Select Id, Contact__c From Target__c Order By Id ASC LIMIT 1]);
        Ctrl.nextEvn = new Event();
        Ctrl.nextEvn.UtilDate__c = system.today();
        ctrl.eventTime = 'Today';
        ctrl.thisEvent = new Event();
        //
        ctrl.thisSideEffects = new Side_Effects__c();
        ctrl.thisSideEffects.comments__c = null;
        ctrl.thisEvent.utilEndTime__c = Time.newInstance(1,2,3,3);
        ctrl.thisEvent.UtilStartTime__c = ctrl.thisEvent.utilEndTime__c.addHours(1);
        ctrl.newEvent.side_Effects__c = true;
        //
        ctrl.SaveEvents();
    }

    @isTest
    public static void test8SaveEvents(){//other errors
        //ctrl instance
        Ctrl_m_MeetingReporting ctrl = new Ctrl_m_MeetingReporting();
        ctrl.selectedcontact = [select Id From Contact Limit 1].Id;
        ctrl.newEvent = new Event();
        ctrl.newEvent.Related_Account__c = [Select Id From Account LIMIT 1].Id;
        ctrl.newEvent.Objective__c = [Select Id From Objective__c LIMIT 1].Id;
        ctrl.selectedConIdForObjective = [select Id From Contact Limit 1].Id;
        ctrl.startsTime = '22:22';
        ctrl.endsTime = '23:23';
        ctrl.selectedTargetsMap = new Map<Id, Target__c>([Select Id, Contact__c From Target__c Order By Id ASC LIMIT 1]);
        Ctrl.nextEvn = new Event();
        Ctrl.nextEvn.UtilDate__c = system.today().addYears(2);///the chaNGE
        ctrl.eventTime = 'Today';
        ctrl.thisEvent = new Event();
        //
        ctrl.thisSideEffects = new Side_Effects__c();
        ctrl.thisSideEffects.comments__c = null;
        ctrl.thisEvent.utilEndTime__c = Time.newInstance(1,2,3,3);
        ctrl.thisEvent.UtilStartTime__c = ctrl.thisEvent.utilEndTime__c.addHours(1);
        ctrl.newEvent.side_Effects__c = true;
        //
        ctrl.SaveEvents();
    }

    @isTest
    public static void test9SaveEvents(){//exception 
        //ctrl instance
        Ctrl_m_MeetingReporting ctrl = new Ctrl_m_MeetingReporting();
        ctrl.selectedcontact = [select Id From Contact Limit 1].Id;
        ctrl.newEvent = new Event();
        ctrl.newEvent.Related_Account__c = [Select Id From AC_Relationship__c LIMIT 1].Id;
        ctrl.newEvent.Objective__c = [Select Id From Objective__c LIMIT 1].Id;
        ctrl.selectedConIdForObjective = [select Id From Contact Limit 1].Id;
        ctrl.startsTime = '22:22';
        ctrl.endsTime = '23:23';
        ctrl.selectedTargetsMap = new Map<Id, Target__c>([Select Id, Contact__c, Related_Account__c, related_Account__r.account__r.city__c, related_Account__r.account__r.territory__c From Target__c Order By Id ASC LIMIT 1]);
        Ctrl.nextEvn = new Event();
        ctrl.thisEvent = null;
        ctrl.newEvent = new Event();
        ctrl.newEvent.Description = 'my supreme description';
        ctrl.newEvent.classification__c = 'A';
        ctrl.selectedMainProduct = 'Product';
        Ctrl.nextEvn.UtilDate__c = System.today();
        ctrl.eventTime = '';
        ctrl.newEvent.Side_Effects__c = true;
        ctrl.newEvent.related_Account__c = [Select Id From AC_Relationship__c LIMIT 1].Id;
       
        ctrl.ac = [Select Id,Account__c,Contact__c,AC_Relationship__c.Account__r.territory__c,account__r.city__c From AC_Relationship__c LIMIT 1];
        
        ctrl.newEvent.objective__c = [Select Id From Objective__c LIMIT 1].Id;
        ctrl.selectedConIdForObjective = [Select Id From Contact LIMIT 1].Id;
        ctrl.thisSideEffects = new Side_Effects__c();
        ctrl.thisSideEffects.Comments__c = 'HALELUIA';
        ctrl.thisSideEffects.sE_Product__c = 'EPIDUO';
        ctrl.thisSideEffects.sE_Way_Of_Use__c = 'Whatever yore doing, make a reverse';
        ctrl.thisSideEffects.sE_Dosage__c = 'The one and only';
        ctrl.thisSideEffects.call_To_Dr_Consent__c = true;
        ctrl.SaveEvents();
    }

    @isTest
    public static void test10SaveEvents(){///////////doesnt reach return to main page
        //ctrl instance
        Ctrl_m_MeetingReporting ctrl = new Ctrl_m_MeetingReporting();
        ctrl.selectedcontact = [select Id From Contact Limit 1].Id;
        ctrl.newEvent = new Event(Location = 'dnhvgdeibg2gh');
        ctrl.newEvent.Related_Account__c = [Select Id From AC_Relationship__c LIMIT 1].Id;
        ctrl.newEvent.Objective__c = [Select Id From Objective__c LIMIT 1].Id;
        ctrl.selectedConIdForObjective = [select Id From Contact Limit 1].Id;
        ctrl.startsTime = '22:22';
        ctrl.endsTime = '23:23';
        ctrl.selectedTargetsMap = new Map<Id, Target__c>([Select Id, Contact__c, Related_Account__c, related_Account__r.account__r.city__c, related_Account__r.account__r.territory__c From Target__c Order By Id ASC LIMIT 1]);
        Ctrl.nextEvn = new Event(Location = 'dnddbgiudebgudebgh');
        ctrl.thisEvent = null;
        ctrl.newEvent = new Event(Location = 'dnhvgdeibgujedbgiudebgudebghfghththth');
        ctrl.newEvent.Description = 'my supreme description';
        ctrl.newEvent.classification__c = 'A';
        ctrl.selectedMainProduct = 'Product';
        Ctrl.nextEvn.UtilDate__c = System.today();
        ctrl.eventTime = '';
        ctrl.newEvent.Side_Effects__c = true;
        ctrl.newEvent.related_Account__c = [Select Id From AC_Relationship__c LIMIT 1].Id;
       
        ctrl.ac = [Select Id,Account__c,Contact__c,AC_Relationship__c.Account__r.territory__c,account__r.city__c From AC_Relationship__c LIMIT 1];
        
        ctrl.newEvent.objective__c = [Select Id From Objective__c LIMIT 1].Id;
        ctrl.selectedConIdForObjective = [Select Id From Contact LIMIT 1].Id;
        ctrl.thisSideEffects = new Side_Effects__c();
        ctrl.thisSideEffects.Comments__c = 'HALELUIA';
        ctrl.thisSideEffects.sE_Product__c = 'EPIDUO';
        ctrl.thisSideEffects.sE_Way_Of_Use__c = 'Whatever yore doing, make a reverse';
        ctrl.thisSideEffects.sE_Dosage__c = 'The one and only';
        ctrl.thisSideEffects.call_To_Dr_Consent__c = true;
        ctrl.nextEvent = new Event(Location = 'dnhvgdeibgujedbgiudebgudebgh');
        ctrl.SaveEvents();
    }

    @isTest
    public static void testGetContactWrp(){
        //ctrl instance
        Ctrl_m_MeetingReporting ctrl = new Ctrl_m_MeetingReporting();
        ctrl.ContactList = null;
        ctrl.getContactWrp([Select Id, Name From Contact]);
    }

    @isTest
    public static void testProperties(){
        //ctrl instance
        Ctrl_m_MeetingReporting ctrl = new Ctrl_m_MeetingReporting();
        ctrl.lastEventsList = new List<Event>();
        ctrl.selectedRelatedAccount = '';
        ctrl.selectedObjective = '';
        ctrl.selectedEventId = (new Event()).Id;
        ctrl.selectedIndex = 1;
        ctrl.mainProductName = 'g';
        ctrl.secondaryProductName = 'h';
        ctrl.thirdProductname = 'p';
    }

    @isTest
    public static void testEvnWrapper(){
        //wrpr evn instance
        Ctrl_m_MeetingReporting.evnWrapper w = new Ctrl_m_MeetingReporting.evnWrapper(new Event(), 1, true);        
    }

    @isTest
    public static void testGoToEvent(){
        //ctrl instance
        Ctrl_m_MeetingReporting ctrl = new Ctrl_m_MeetingReporting();
        ctrl.eventTime = 'FutureDate';
        ctrl.GoToEvent();
        ctrl.eventTime = 'Today';
        Event ev = new Event(Classification__c = null, StartDateTime = DateTime.newInstance(1997, 1, 31, 7, 8, 16), EndDateTime = DateTime.newInstance(1998, 1, 31, 7, 8, 16));
        ctrl.evnWrprList = new List <Ctrl_m_MeetingReporting.evnWrapper>();
        ctrl.evnWrprList.add(new Ctrl_m_MeetingReporting.evnWrapper(ev, 1, true));
        ctrl.selectedEventId = ev.Id;
        ctrl.GoToEvent();
        ctrl.eventTime = 'PastDate';
        ctrl.GoToEvent();
        ev.Side_Effect__c = (new Side_Effects__c().Id);
        ctrl.GoToEvent();
    }

    @isTest
    public static void testDeleteEvent(){
        //ctrl instance
        Ctrl_m_MeetingReporting ctrl = new Ctrl_m_MeetingReporting();
        ctrl.evn = new Event(UtilDate__c = null);
        Event ev = new Event(Classification__c = null, StartDateTime = DateTime.newInstance(1997, 1, 31, 7, 8, 16), EndDateTime = DateTime.newInstance(1998, 1, 31, 7, 8, 16));
        ctrl.evnWrprList = new List <Ctrl_m_MeetingReporting.evnWrapper>();
        ctrl.evnWrprList.add(new Ctrl_m_MeetingReporting.evnWrapper(ev, 1, true));
        ctrl.selectedEventId = ev.Id;
        ctrl.DeleteEvent();
        ctrl.evnWrprList = new List <Ctrl_m_MeetingReporting.evnWrapper>();
        ctrl.DeleteEvent();
        //insert ev;
        //ctrl.DeleteEvent();
    }

    @isTest
    public static void testReturnToMainPageAndProductLists(){ //event time = future date
        //ctrl instance
        Ctrl_m_MeetingReporting ctrl = new Ctrl_m_MeetingReporting();
        ctrl.ReturnToMainPage();
        ctrl.getMainProductList();
        ctrl.getSecondaryProductList();
        ctrl.getThirdProductList();
    }

    @isTest
    public static void testShowLastEvents(){
        //ctrl instance
        Ctrl_m_MeetingReporting ctrl = new Ctrl_m_MeetingReporting();
        Event ev = new Event(startDateTime = system.today().addDays(-10),description= '', whoId = [select Id From Contact LIMIT 1].Id);
        ctrl.evnWrprList = new List <Ctrl_m_MeetingReporting.evnWrapper>();
        ctrl.evnWrprList.add(new Ctrl_m_MeetingReporting.evnWrapper(ev, 1, true));
        ctrl.selectedEventId = ev.Id;
        ctrl.ShowLastEvents();
    }

    @isTest
    public static void testCloseModal(){
        //ctrl instance
        Ctrl_m_MeetingReporting ctrl = new Ctrl_m_MeetingReporting();
        ctrl.closeModalStatus = '';
        ctrl.Closemodal();
        ctrl.closeModalStatus = 'Selected';
        ctrl.newEvent = new Event();
        ctrl.selectedRelatedAccount = [Select Id From AC_Relationship__c LIMIT 1].Id;
        ctrl.selectedObjective = [Select Id From Objective__c LIMIT 1].Id;
        ctrl.Closemodal();
        ctrl.selectedRelatedAccount = null;
        ctrl.newEvent = new Event(Location = 'ggggggg');
        ctrl.Closemodal();
    }

    @isTest
    public static void testUpdateEvent(){
         //ctrl instance
        Ctrl_m_MeetingReporting ctrl = new Ctrl_m_MeetingReporting();
        ctrl.UpdateEvent();
        ctrl.nextEvn = new Event();
        ctrl.startsTime = '22222222222222';
        ctrl.UpdateEvent();
        ctrl.startsTime = '23:24';
        ctrl.endsTime = '23:23';
        ctrl.thisEvent = new Event();
        ctrl.UpdateEvent();
}

}