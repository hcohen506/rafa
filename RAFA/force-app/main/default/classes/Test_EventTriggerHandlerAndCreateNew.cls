/*
 ---- Apex Code Coverage for: ----
Ctrl_CreateNewEvents
EventTrigger
EventTriggerHandler
Ctrl_EventMeetingGoogleMaps
*/

@isTest
public class Test_EventTriggerHandlerAndCreateNew {
    @isTest static void EvnTriggerHandlerAndCreateNew() {
        Cls_ObjectCreator cls = new Cls_ObjectCreator();
        Profile profile = cls.createProfile('Standard User');
        User ManagerUser = cls.createUser(profile,null);
        User usr = cls.createUserWithManager(profile,null,ManagerUser);
        //Objective__c objective = cls.createObjective(usr);
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];  
        Objective__c objective = cls.createObjective(thisUser);
        Account acc = cls.CreateParentAccount ();
        Account acc2 = cls.CreateAccountWithParentAccount (acc);
        Contact con = cls.createContact (acc2);     
        AC_Relationship__c acRelationship = cls.createAc_Relationship (acc2, con);
        
        //Target__c target = cls.createtarget(objective,usr,acRelationship);
        //Event evn = cls.createEventCompleted(target,'Meeting',usr.id);
        //Event evn2 = cls.createEventCompleted(target,'Meeting',usr.id);
        //Event evn3 = cls.createEventCompleted(target,'Meeting',usr.id);
        
        Target__c target = cls.createtarget(objective,thisUser,acRelationship);       
        Event evn = cls.createEventCompleted(target,'Meeting',thisUser.id);
        evn.Classification__c = 'A';
        update evn;
        Event evn2 = cls.createEventCompleted(target,'Meeting',thisUser.id);
        Event evn3 = cls.createEventCompleted(target,'Meeting',thisUser.id);
        //createEventWithRTAndUserId(string  RcdtypeName, id usrId) {
        //system.debug('*** target  L '+target );      

        Test.startTest();
            System.runAs (usr) {         
        
                Ctrl_CreateNewEvents ctrl99 = new Ctrl_CreateNewEvents();  
                ctrl99.getItems2();
                ctrl99.setParamy(); 
                ctrl99.getItems1(); 
           
                ctrl99.getExpertiesList();
                ctrl99.getTerritoryList();
                ctrl99.getClassificationList();
              //  ctrl99.getInterestsList();                                   
                ctrl99.GetDayOfWeek();
                ctrl99.trg = target;
                ctrl99.selectedDate = Date.today().addDays(-1);
                system.debug('*** TEST - selectedDate: ' + ctrl99.selectedDate);
                ctrl99.GetDayOfWeek();
                ctrl99.Search();
                ctrl99.CreateEvent();
                //Ctrl_CreateNewEvents.trgWrapper ctrl2 = new  Ctrl_CreateNewEvents.trgWrapper(target,true);               
                Ctrl_CreateNewEvents.trgWrapper ctrl2 = new  Ctrl_CreateNewEvents.trgWrapper(target,true,true);               
                Ctrl_CreateNewEvents ctrl = new Ctrl_CreateNewEvents();
                ctrl.selectedDate =  Date.today();
                 system.debug('*** TEST - selectedDate2: ' + ctrl.selectedDate);
                /*
                for (Ctrl_deleteParticipants.ConfPartWrpr ConfWrpr : ctrl_delete2.confPartWrprList ) {
                    ConfWrpr.isSelected = true;
                }
                ctrl_delete2.DeleteConfParticipant();
                */
                //for (Ctrl_CreateNewEvents.trgWrapper Wrpr : ctrl.trgWrpList) {
                //    Wrpr.isSelected = true;
                //} 

                ctrl.toSelectExpertise = 'PCR';
               // ctrl.toSelectTerritory = ;
                ctrl.toSelectClassification = 'C';
                //ctrl.toSelectInterests = 'CNV';
                ctrl.Search();  
                for (Ctrl_CreateNewEvents.trgWrapper Wrpr : ctrl.trgWrpList) {
                    Wrpr.isSelected = true;
                    Wrpr.isIdentical = true;
                }  
                ctrl.toSelectClassification = 'A';                   
                ctrl.Search();    
                ctrl.CreateEvent();            
                ctrl.accCon = acRelationship;
                ctrl.Search();  
                Time myTime1 = Time.newInstance(10, 30, 2, 20); 
                Time myTime2 = Time.newInstance(11, 30, 2, 20); 
                acRelationship.Sunday_From_1__c = myTime2 ; 
               // acRelationship.Sunday_to_1__c= ; 
                update acRelationship;
                ctrl.Search();   
                acRelationship.Sunday_to_1__c= myTime1 ; 
                update acRelationship;
                ctrl.Search(); 
                acRelationship.Sunday_From_1__c = myTime1 ; 
                acRelationship.Sunday_to_1__c= myTime2 ; 
                update acRelationship;
                ctrl.Search(); 
                ctrl.CreateEvent();       
                 
                Ctrl_CreateNewEvents ctrl3 = new Ctrl_CreateNewEvents();
            }
 /// ====== Ctrl_EventMeetingGoogleMaps  ======= ///
            //System.runAs (ManagerUser) {               
            System.runAs (thisUser) {   
                Ctrl_EventMeetingGoogleMaps ctrlEvnMtGgl = new Ctrl_EventMeetingGoogleMaps ();
                ctrlEvnMtGgl.ShowMap();
                ctrlEvnMtGgl.meetingDate = system.today();
                ctrlEvnMtGgl.ShowMap();
            }
            System.runAs (usr) { 
                Ctrl_EventMeetingGoogleMaps ctrlEvnMtGgl2 = new Ctrl_EventMeetingGoogleMaps ();
                ctrlEvnMtGgl2.ShowMap();
                ctrlEvnMtGgl2.meetingDate = system.today();
                ctrlEvnMtGgl2.ShowMap();
            }

        Test.stopTest();


        
    }
}