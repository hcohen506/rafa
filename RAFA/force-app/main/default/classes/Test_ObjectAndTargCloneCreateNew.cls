/*
 ---- Apex Code Coverage for: ----
Ctrl_ObjectivesClone
Ctrl_TargetsClone
Ctrl_CreateNewTargets
*/
@isTest
public class Test_ObjectAndTargCloneCreateNew {
    @isTest static void Test_testing() {
        Cls_ObjectCreator cls = new Cls_ObjectCreator();
        Profile profile = cls.createProfile('Standard User');
        UserRole usrRole = cls.createUserRole('Representative');
        UserRole usrRole1 = cls.createUserRole('Team Leader');
        UserRole usrRole2 = cls.createUserRole('Manager');
        User usr = cls.createUser(profile,null);
        //UserRole userRole = cls.createUserRole('testUserRole'); // Mixed-DML Exception
        User usr2 = cls.createUser(profile,null);
        User systemAdministratorUser = cls.createSystemAdministratorUser();
        Account acc = cls.CreateParentAccount ();
        Account acc2 = cls.CreateAccountWithParentAccount (acc);
        Contact con = cls.createContact (acc2);     
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];  
        //Objective__c objective = cls.createObjective(usr);
        Objective__c objective = cls.createObjective(thisUser);
        AC_Relationship__c acRelationship = cls.createAc_Relationship (acc2, con);
        Target__c target = cls.createtarget(objective,usr,acRelationship);

        Test.startTest();

//---- Apex Code Coverage for: Ctrl_ObjectivesClone ----

            system.runAs(usr2){
                Ctrl_ObjectivesClone ctrl2 = new Ctrl_ObjectivesClone();
            }
            Ctrl_ObjectivesClone ctrl = new Ctrl_ObjectivesClone();
            ctrl.getYearsFromList(); 
            ctrl.getYearsToList();
            ctrl.getQrList();
            ctrl.getUsersList();
            ctrl.Search();
            ctrl.CloneObjectives();
            date dt = system.today();
            ctrl.toSelectYear = string.valueof(dt.year());
            ctrl.toSelectQuarter = '1';
            //ctrl.toSelectUser = usr.id;
            //ctrl.toSelectUser = thisUser.id;
            ctrl.Search();
            ctrl.toInsertYear = string.valueof(dt.year());
            ctrl.toInsertQuarter = '2';
            //ctrl.toInsertUser = usr.id;
            //ctrl.currentUser = thisUser;
            Ctrl_ObjectivesClone.ObjWrapper testWrap = new Ctrl_ObjectivesClone.ObjWrapper(objective,true); 
            for (Ctrl_ObjectivesClone.ObjWrapper  objWrp : ctrl.ojbWrpLst) {
                objWrp.isSelected = false;
            }
            ctrl.CloneObjectives();
            for (Ctrl_ObjectivesClone.ObjWrapper  objWrp : ctrl.ojbWrpLst) {
                objWrp.isSelected = true;
            }
            ctrl.CloneObjectives();

//---- Apex Code Coverage for: Ctrl_TargetsClone ----

            Ctrl_TargetsClone trgClctrl = new Ctrl_TargetsClone();
            Ctrl_TargetsClone.targetWrapper trgtWrapper = new Ctrl_TargetsClone.targetWrapper(target,true);
            trgClctrl.getYearsFromList(); 
            trgClctrl.getYearsToList();
            trgClctrl.getQrList();
            trgClctrl.getUsersList();
            trgClctrl.Search();
            trgClctrl.CloneTargets();
            trgClctrl.toSelectYear = string.valueof(dt.year());
            trgClctrl.toSelectQuarter = '1';
            trgClctrl.toSelectUser = usr.id;
            trgClctrl.toInsertYear = string.valueof(dt.year());
            trgClctrl.toInsertQuarter = '1';
            trgClctrl.toInsertUser = usr.id;
            trgClctrl.Search();
            trgClctrl.objectiveLookUp.Objective__c = objective.id;
            for (Ctrl_TargetsClone.targetWrapper  trgWrp : trgClctrl.targetWrpList) {
                trgWrp.isSelected = false;
            }
            trgClctrl.CloneTargets();
            for (Ctrl_TargetsClone.targetWrapper  trgWrp : trgClctrl.targetWrpList) {
                trgWrp.isSelected = true;
            }
            trgClctrl.CloneTargets();
            trgClctrl.toSelectExperties = 'PCR';
            trgClctrl.Search();
            for (Ctrl_TargetsClone.targetWrapper  trgWrp : trgClctrl.targetWrpList) {
                trgWrp.isSelected = true;
            }
            trgClctrl.CloneTargets();


//---- Apex Code Coverage for: Ctrl_CreateNewTargets ----
            Test.setCurrentPageReference(new PageReference('Page.VF_CreateNewTargets')); 
            System.currentPageReference().getParameters().put('objectiveId',objective.Id );    
            Ctrl_CreateNewTargets crtNewTrgCtrl = new Ctrl_CreateNewTargets();
            Ctrl_CreateNewTargets.AccConWrapper accCnWrprCtrl = new Ctrl_CreateNewTargets.AccConWrapper(acRelationship, true,0,true);
            crtNewTrgCtrl.getExpertiesList();
                //  crtNewTrgCtrl.accLookUp = acRelationship;
                // crtNewTrgCtrl.accLookUp.util_toAccount__c  = acRelationship.Account__c;
                //  crtNewTrgCtrl.parentaccLookUp.util_toAccount__c  = acc.id;
            crtNewTrgCtrl.getSubscriptionPotentialList();
            crtNewTrgCtrl.getClinicSizeList();
            crtNewTrgCtrl.getUtilizationLevelList();
            crtNewTrgCtrl.getClassificationList();
            crtNewTrgCtrl.getPositionList();
            crtNewTrgCtrl.getTerritoryList();
            crtNewTrgCtrl.Search();
            crtNewTrgCtrl.CreateTarget();
            crtNewTrgCtrl.toSelectExpertise = 'Internist';                      
            crtNewTrgCtrl.getItems2();
            crtNewTrgCtrl.setParamy(); 
            crtNewTrgCtrl.getItems1(); 
             
            crtNewTrgCtrl.Search();
            crtNewTrgCtrl.CreateTarget();

            Ctrl_CreateNewTargets crtNewTrgCtrl2 = new Ctrl_CreateNewTargets();
            crtNewTrgCtrl2.toSelectExpertise = 'PCR';
            crtNewTrgCtrl2.Search();
            crtNewTrgCtrl2.CreateTarget();

            Ctrl_CreateNewTargets.AccConWrapper accCnWrprCtrl2 = new Ctrl_CreateNewTargets.AccConWrapper(acRelationship, true,2,true);
            for (Ctrl_CreateNewTargets.AccConWrapper accWrp : crtNewTrgCtrl2.accConWrpList) {
                accWrp.isSelected = false;
            }
            crtNewTrgCtrl2.CreateTarget();
            for (Ctrl_CreateNewTargets.AccConWrapper  accWrp : crtNewTrgCtrl2.accConWrpList) {
                accWrp.isSelected = true;
            }
            crtNewTrgCtrl2.CreateTarget();
            for (Ctrl_CreateNewTargets.AccConWrapper  accWrp : crtNewTrgCtrl2.accConWrpList) {
                accWrp.isSelected = true;
                accWrp.requiredNoOfVisits= 2;
            }
            crtNewTrgCtrl2.CreateTarget();

    ////---- Apex Code Coverage for: Ctrl_TargetsToLocked ----
    //      usr.userroleid = usrRole.id;
    //      update usr;
    //      system.runAs(usr){
    //          Ctrl_TargetsToLocked crtTrgToLck  = new Ctrl_TargetsToLocked();
    //      }
    //      Ctrl_TargetsToLocked crtTrgToLck2  = new Ctrl_TargetsToLocked();
    //      crtTrgToLck2.getQrList();
    //      crtTrgToLck2.getYearList();
    //      crtTrgToLck2.getRoleList();
    //      crtTrgToLck2.Lock();
            
        Test.stopTest();

        
    }
}