@isTest
private class Test_ObjectivesClone {    
    @isTest static void ObjectivesCloneTest() {
        Cls_ObjectCreator cls = new Cls_ObjectCreator();

        Test.startTest();
            Ctrl_ObjectivesClone ctrl = new Ctrl_ObjectivesClone();
            ctrl.getYearsFromList(); 
            ctrl.getYearsToList();
            ctrl.getQrList();
            ctrl.getUsersList();
            ctrl.Search();
            ctrl.CloneObjectives();
        Test.stopTest();
    }
}