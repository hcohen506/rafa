@isTest
public class Test_TargetsToLocked {
    @isTest static void TargetsToLocked_testing() {

        Cls_ObjectCreator cls = new Cls_ObjectCreator();

        Profile profile = cls.createProfile('Standard User');
        //UserRole usrRole = cls.createUserRole('Representative');
        //UserRole usrRole1 = cls.createUserRole('Team Leader');
        //UserRole usrRole2 = cls.createUserRole('Manager');
        User usr = cls.createUser(profile,'Representative');
        //UserRole userRole = cls.createUserRole('testUserRole'); // Mixed-DML Exception
        User usr2 = cls.createUser(profile,'Manager');
        User usr3 = cls.createUser(profile,'Team Leader');
        User systemAdministratorUser = cls.createSystemAdministratorUser();
        Account acc = cls.CreateParentAccount ();
        Account acc2 = cls.CreateAccountWithParentAccount (acc);
        Contact con = cls.createContact (acc2);  
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];     
        //Objective__c objective = cls.createObjective(usr);
        Objective__c objective = cls.createObjective(thisUser);
        AC_Relationship__c acRelationship = cls.createAc_Relationship (acc2, con);
        Target__c target = cls.createtarget(objective,usr3,acRelationship);
        //id targetLockRecordTypeId = Schema.SObjectType.Target__c.getRecordTypeInfosByName().get('Open Target').getRecordTypeId();
        //target.recordtypeid = targetLockRecordTypeId;
        //update target;

        Test.startTest();

        //---- Apex Code Coverage for: Ctrl_TargetsToLocked ----
            //usr.userroleid = usrRole.id;
            //update usr;
            Ctrl_TargetsToLocked crtTrgToLck2  = new Ctrl_TargetsToLocked();
            crtTrgToLck2.getQrList();
            crtTrgToLck2.getYearList();
            crtTrgToLck2.getRoleList();
            crtTrgToLck2.Lock();
            system.runAs(usr){
                Ctrl_TargetsToLocked crtTrgToLck  = new Ctrl_TargetsToLocked();
                crtTrgToLck.Lock();
            }
            system.runAs(usr2){
                Ctrl_TargetsToLocked crtTrgToLck1  = new Ctrl_TargetsToLocked();
                crtTrgToLck1.Lock();
            }
            // IF needed more coverage then -> create the role hierarchy itself
            system.runAs(usr3){
                date dt = system.today();
                Ctrl_TargetsToLocked crtTrgToLck3  = new Ctrl_TargetsToLocked();
                crtTrgToLck3.getQrList();
                crtTrgToLck3.getYearList();
                crtTrgToLck3.getRoleList();
                crtTrgToLck3.selectYear =  string.valueof(dt.year());
                crtTrgToLck3.selectQuarter = '1';
                crtTrgToLck3.selectRole = usr3.UserRoleid;
                crtTrgToLck3.Lock();
            }
        Test.stopTest();


    }
}