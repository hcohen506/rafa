/*
 ---- Apex Code Coverage for: ----
Ctrl_addParticipants
Ctrl_deleteParticipants
*/
@isTest
public class Test_addAndDeleteParticipants {
    @isTest static void Test_addAndDeleteParticipants() {
        Cls_ObjectCreator cls = new Cls_ObjectCreator();
        Account acc = cls.CreateParentAccount ();
        Account acc2 = cls.CreateAccountWithParentAccount (acc);
        Contact con = cls.createContact (acc2);     
        Contact con2 = cls.createContact (acc2);        
        //AC_Relationship__c acRelationship = cls.createAc_Relationship (acc2, con);
        Conference__c conf = cls.creatConference();
        Conference__c conf2 = cls.creatConference();
        //Conf_Participant__c ConfParcls = cls.creatConferenceParticipant(con,conf);
        //Conf_Participant__c ConfParcls = cls.creatConferenceParticipant(con2,conf2);


        Test.startTest();

    //---- Apex Code Coverage for: Ctrl_addParticipants ---- //

            Test.setCurrentPageReference(new PageReference('Page.VF_addParticipants')); 
            System.currentPageReference().getParameters().put('conferenceId',conf2.Id );  
            Ctrl_addParticipants ctrl99 = new Ctrl_addParticipants();
            System.currentPageReference().getParameters().put('conferenceId',conf.Id );  
            Ctrl_addParticipants ctrl = new Ctrl_addParticipants();
            ctrl.addParticipants();
            ctrl.getInterestsList();
            ctrl.getClassificationList();
            ctrl.getExpertiesList();
            ctrl.getBelongstoList();
            //ctrl.toSelectInterests.add ('CNV');
            //Ctrl_addParticipants.AccConWrapper ctrl2 = new  Ctrl_addParticipants.AccConWrapper(acRelationship,true);
            ctrl.Search();
            ctrl.addParticipants();
            for (Ctrl_addParticipants.AccConWrapper accwrp : ctrl.accConWrpLst ) {
                accWrp.isSelected = true;
            }
            ctrl.Search();
            ctrl.addParticipants();
            
            AC_Relationship__c acRelationship2 = cls.createAc_Relationship (acc2, con);
           // system.debug('*** acRelationship :'+acRelationship );
            ctrl.toSelectClassification = 'A';
            ctrl.toSelectExpertise = 'PCR';
            ctrl.toSelectBelongsto = 'לאומית';
            ctrl.Search();
            for (Ctrl_addParticipants.AccConWrapper accwrp : ctrl.accConWrpLst ) {
                accWrp.isSelected = false;
            }
            ctrl.addParticipants();
            
           // AC_Relationship__c acRelationship = cls.createAc_Relationship (acc2, con);
         //   system.debug('*** acRelationship :'+acRelationship );
            ctrl.Search();
            for (Ctrl_addParticipants.AccConWrapper accwrp : ctrl.accConWrpLst ) {
                accWrp.isSelected = true;
            }
            ctrl.addParticipants();
            

        //---- Apex Code Coverage for: Ctrl_deleteParticipants ---- //
            
            Ctrl_deleteParticipants ctrl_delete1 = new Ctrl_deleteParticipants ();
            ctrl_delete1.DeleteConfParticipant();

            Test.setCurrentPageReference(new PageReference('Page.VF_deleteParticipants')); 
            System.currentPageReference().getParameters().put('conferenceId',conf2.Id );  
            Ctrl_deleteParticipants ctrl_delete99 = new Ctrl_deleteParticipants();
            System.currentPageReference().getParameters().put('conferenceId',conf.Id ); 
            Ctrl_deleteParticipants ctrl_delete = new Ctrl_deleteParticipants ();
            ctrl_delete.DeleteConfParticipant();



            Conf_Participant__c ConfPart = cls.creatConferenceParticipant(con2,conf);
            Ctrl_deleteParticipants ctrl_delete2 = new Ctrl_deleteParticipants ();

            Ctrl_deleteParticipants.ConfPartWrpr ctrl2_deletewrp = new  Ctrl_deleteParticipants.ConfPartWrpr(ConfPart,true);
            
            for (Ctrl_deleteParticipants.ConfPartWrpr ConfWrpr : ctrl_delete2.confPartWrprList ) {
                ConfWrpr.isSelected = true;
            }
            ctrl_delete2.DeleteConfParticipant();
            

        Test.stopTest();

        
    }
}