public class test1{
    public Map<Id,List<myWrp>> ChildParentMap {get;set;}
    public List<myWrp> ParentList {get;set;}
    public List<myWrp> ChildList {get;set;}
    public String selectedParent {get;set;}

    public test1(){
        ChildParentMap = new Map<Id,List<myWrp>>();
        ParentList =  new List<myWrp>();
        ChildList = new List<myWrp>();
        List<Account> fullList = [SELECT Id, Name,ParentId FROM account];
        for (account a : fullList) {
            if(a.ParentId == null){
                if(!ChildParentMap.containsKey(a.id)){
                    ChildParentMap.put(a.id, new List<myWrp>());
                }
                ParentList.add(new myWrp(a));
            }else{
                if(ChildParentMap.containsKey(a.ParentId)){
                    ChildParentMap.get(a.ParentId).add(new myWrp(a));
                }else{
                    ChildParentMap.put(a.ParentId, new List<myWrp>());
                    ChildParentMap.get(a.ParentId).add(new myWrp(a));
                }
                ChildList.add(new myWrp(a));
            }
        }
        System.debug('childParent Map:' +ChildParentMap);
    }


    public string getItems2() {
        // return JSON.serialize(getChildWrp([SELECT Id, Name FROM account WHERE ParentId != null]));
        return JSON.serialize(getChildWrp());

    }

    public void setParamy() {
       System.debug(selectedParent);
    }

    public string getItems1() {
        return JSON.serialize(getParentWrp([SELECT Id, Name FROM account WHERE ParentId = null]));
    }

    public string getItems3(String Idy) {

        return JSON.serialize(ChildParentMap.get(Idy));
    }
    public class myWrp{
        id id;
        string name;
        public myWrp(account a){
            this.id = a.id;
            this.name =  a.name;
        }
    }
    public list<myWrp> getParentWrp (list<account> accList){
        if(ParentList == null){
            ParentList =  new  list<myWrp>();
            for(account a:accList){
                ParentList.add(new myWrp(a));
            }
        }
        return ParentList;
    }

    public list<myWrp> getChildWrp (){
        System.debug('inside Child, parent is:'+ selectedParent);
        if(selectedParent == null || selectedParent == '' ){
            return ChildList;
        }else{
            System.debug('ChildParentMap.get(selectedParent):'+ ChildParentMap.get(selectedParent));
            return ChildParentMap.get(selectedParent);
        }

    }

}